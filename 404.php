<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<base href="<?php echo home_url() ?>"></base>
	<title>404 - Page Not Found: <?php echo get_bloginfo( 'title' ) ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta name="cms-area" content="OneOff" />
	<meta name="cms-all" content="All" />
	<meta name="description" content="Sorry, your page was not found" />
	<style type="text/css">
		/* Reset CSS */

		html, body, div, span, applet, object, iframe,
		h1, h2, h3, h4, h5, h6, p, blockquote, pre,
		a, abbr, acronym, address, big, cite, code,
		del, dfn, em, font, img, ins, kbd, q, s, samp,
		small, strike, strong, sub, sup, tt, var,
		b, u, i, center,
		dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td {
			margin:         0;
			padding:        0;
			border:         0;
			outline:        0;
			font-size:      100%;
			vertical-align: baseline;
			background:     transparent;
		}

		body {
			line-height: 1;
		}

		ol, ul {
			list-style: none;
		}

		blockquote, q {
			quotes: none;
		}

		blockquote:before, blockquote:after,
		q:before, q:after {
			content: '';
			content: none;
		}

		/* remember to define focus styles! */
		:focus {
			outline: 0;
		}

		/* remember to highlight inserts somehow! */
		ins {
			text-decoration: none;
		}

		del {
			text-decoration: line-through;
		}

		/* tables still need 'cellspacing="0"' in the markup */
		table {
			border-collapse: collapse;
			border-spacing:  0;
		}

		body, html {
			width: 100%;
		}

		body {
			background-image:    url(http://www.unr.edu/prebuilt/images/pages/wolfie_bg2.jpg);
			background-repeat:   no-repeat;
			background-position: center top;
			background-color:    #ffffff;
		}

		#MainBody {
			width:       800px;
			margin:      350px auto 0px auto;
			color:       #336699;
			font-family: "century gothic", "lucida sans unicode", "lucida grande", sans-serif;
			font-size:   1em;
		}

		#MainBody h1 {
			font-size: 1.4em;
		}

		#MainBody ul {
			list-style-type:     disc;
			list-style-position: outside;
			text-indent:         0px;
			padding-left:        1em;
		}

		#MainBody li {
			padding-bottom: 3px;
		}

		#MainBody a:hover {
			text-decoration: none;

		}

		#cref_iframe {
			width:      400px;
			margin:     20px auto;
			text-align: center;
		}

		#Logo {
			margin: 10px auto 0px auto;
			width:  150px;
		}
		form{
			text-align: center;
			margin-top: 30px;
		}
		.search-wrapper {
			position: relative;
			display: inline-block;
			*display: inline;
			zoom: 1;
			margin-bottom: 15px;
		}
		.overlay-icon {
			position: absolute;
			left: 12px;
			top: 50%;
			font-size: 2em;
			margin-top: -16px;
			color: #ddd;
		}
		label {
			font-size: 1.125em;
			color: #828282;
			font-family: "Myriad W08 Semibold", "Helvetica Neue", Helvetica, Arial, sans-serif;
			display: block;
			margin-bottom: 0.8333333333333334em;
		}
		.search-large {
			border: 1px solid #d2d2d2;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			-moz-background-clip: padding;
			-webkit-background-clip: padding-box;
			background-clip: padding-box;
			font-size: 1.125em;
			padding: .6em .8em .6em .8em;
			width: 23em;
			height: auto;
			background: #fff;
			line-height: normal;
		}
		.search-btn {
			opacity: 1;
			background-color: #003366;
			color: #ffffff;
			position: absolute;
			top: 6px;
			right: 6px;
			bottom: 5px;
			border: none;
			padding: 0 .42em;
			zoom: 1;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			-moz-background-clip: padding;
			-webkit-background-clip: padding-box;
			background-clip: padding-box;
			font-size: 1.375em;
			-webkit-transition: background-color 0.3s ease-in-out;
			-moz-transition: background-color 0.3s ease-in-out;
			-ms-transition: background-color 0.3s ease-in-out;
			-o-transition: background-color 0.3s ease-in-out;
			transition: background-color 0.3s ease-in-out;
			cursor: pointer;
		}
	</style>

</head>
<body id="x67">
<div id="MainBody">
	<h1>Please try the following:</h1>
	<ul>
		<li>Make sure that the website address displayed in the address bar of your browser is spelled and formatted correctly.</li>
		<li>If you reached this page by clicking a link, contact the website publisher to alert them that the link is incorrectly formatted.</li>
		<li>Try browsing the
			<a href="<?php echo home_url() ?>/site-map">Site Map</a> to find the information you need or try searching
		</li>
	</ul>
	<?php get_search_form() ?>

	<div id="Logo"><a style="display: block; width: 150px; height: 110px; overflow:hidden;" href="<?php echo home_url() ?>">
			<img style="height: 459%;" alt="University of Nevada, Reno" height="110" src="http://wpdemo.dev/topica/wp-content/uploads/2017/01/logo-sprites.png" width="150" /></a>
	</div>
</div>
</body>
</html>
