<?php get_header(); ?>
<?php
$prefix             = 'topica_';
$layout_style       = 'layout-2';
$page_sidebar       = 'right';
$page_left_sidebar  = rwmb_meta( $prefix . 'page_left_sidebar' );
$page_right_sidebar = 'chuynngnh';
$page_class_extra   = rwmb_meta( $prefix . 'page_class_extra' );
?>
<?php
$content_col_number = 12;
if ( is_active_sidebar( $page_left_sidebar ) && ( ( $page_sidebar == 'both' ) || ( $page_sidebar == 'left' ) ) ) {
	$content_col_number -= 3;
}
if ( is_active_sidebar( $page_right_sidebar ) && ( ( $page_sidebar == 'both' ) || ( $page_sidebar == 'right' ) ) ) {
	$content_col_number -= 3;
}

$content_col = 'col-sm-' . $content_col_number;
if ( ( $content_col_number == 12 ) && ( $layout_style == 'full' ) ) {
	$content_col = '';
}
if ( $content_col_number < 12 ) {
	$main_class[] = 'has-sidebar';
}
$content_col = 'col-sm-' . $content_col_number;
if ( ( $content_col_number == 12 ) && ( $layout_style == 'full' ) ) {
	$content_col = '';
}
$sidebar_col = 'col-md-3';
$main_class  = array( 'site-content-page' );
?>
<?php if ( $layout_style == '-1' || $layout_style == 'layout-1' ) : ?>
	<div id="main-content">
		<section class="content-wrapper" aria-label="Main body of content">
			<header>
				<div class="header-seperator">
					<h1><strong><?php the_title() ?></strong></h1>
					<div class="seperator-icon"><span class="icon-wolf"></span>
					</div>
				</div>
				<div class="container">
					<?php topica_the_breadcrumb(); ?>
				</div>
			</header>
			<div class="container">
				<div class="row">
					<div id="maincontent" style="padding-right: 10px; padding-left: 10px">
						<div class="row <?php echo join( ' ', $main_class ) ?>">
							<?php if ( is_active_sidebar( $page_left_sidebar ) && ( $page_sidebar == 'left' ) ): ?>
								<nav class="side-bar-front left-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
									<?php dynamic_sidebar( $page_left_sidebar ); ?>
								</nav>
							<?php endif; ?>
							<article class="content <?php echo esc_attr( $content_col ) ?>">
								<?php the_content() ?>
							</article>

							<?php if ( is_active_sidebar( $page_right_sidebar ) && ( $page_sidebar == 'right' ) ): ?>
								<nav class="side-bar-front side-bar right-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
									<?php dynamic_sidebar( $page_right_sidebar ); ?>
								</nav>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>

		</section>
	</div>
<?php else : ?>
	<div class="row" id="main-content">
		<div class="container">
			<div class="row hidden-print" id="Breadcrumbs">
				<div class="container">
					<?php topica_the_breadcrumb(); ?>
				</div>
			</div>

			<div class="row <?php echo join( ' ', $main_class ) ?>">
				<?php if ( is_active_sidebar( $page_left_sidebar ) && ( $page_sidebar == 'left' ) ): ?>
					<nav class="side-bar-front left-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
						<?php dynamic_sidebar( $page_left_sidebar ); ?>
					</nav>
				<?php endif; ?>
				<article class="content <?php echo esc_attr( $content_col ) ?>">
					<?php the_content() ?>
				</article>

				<?php if ( is_active_sidebar( $page_right_sidebar ) && ( $page_sidebar == 'right' ) ): ?>
					<nav class="side-bar-front right-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
						<?php dynamic_sidebar( $page_right_sidebar ); ?>
					</nav>
				<?php endif; ?>
			</div>

		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>
