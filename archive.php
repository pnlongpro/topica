<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/11/2017
 * Time: 3:24 AM
 */
get_header();
?>
<div class="vlog-section ">
	<div class="container">

		<div class="vlog-content">
			<div class="row">
				<div class="vlog-module module-posts col-lg-12">

					<div class="vlog-mod-head">
						<div class="vlog-mod-title">
							<h4 style="font-family:verdana;"><?php echo single_cat_title(); ?></h4></div>
					</div>
					<div class="row vlog-posts row-eq-height vlog-posts">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'template-parts/content', 'archive' ) ?>
						<?php endwhile; ?>
					</div>
					<div class="blog-paging">
						<?php echo topica_paging_nav(); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="vlog-sidebar vlog-sidebar-right">
			<?php dynamic_sidebar('widget-area-1') ?>
		</div>

	</div>
</div>
<?php get_footer() ?>
