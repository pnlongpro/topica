<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/11/2017
 * Time: 12:06 AM
 */

?>
<?php
	$classed = array();
	$classed[] = 'vlog-lay-b lay-horizontal vlog-post';
?>
<article <?php post_class($classed) ?>>
	<div class="row">
		<div class="col-lg-6 col-md-6  col-sm-6 col-xs-12">
			<?php if( has_post_thumbnail() ) : ?>
			<div class="entry-image">
				<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
					<?php
						$thumbnail_url = '';
						$width = 366;
						$height = 205;
						$image = get_post_thumbnail_id( get_the_ID() );
						if (!empty($image)) {
							$images_attr = wp_get_attachment_image_src($image, "full");
							if (isset($images_attr)) {
								$resize = matthewruddy_image_resize($images_attr[0], $width, $height);
								if ($resize != null)
									$thumbnail_url = $resize['url'];
							}
						}
					?>
					<img src="<?php echo $thumbnail_url ?>" class="attachment-vlog-lay-b size-vlog-lay-b wp-post-image" alt="<?php the_title() ?>" >
					<span class="vlog-format-label medium"><?php echo get_post_format() ?></span> </a>
			</div>
			<?php endif; ?>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="entry-header">
				<span class="entry-category"><?php echo get_the_category_list(' / '); ?></span>
				<h2 class="entry-title h2">
					<a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
				<?php get_template_part('template-parts/post','meta') ?>

			</div>
			<div class="entry-content">
				<?php
				$content           = get_the_content();
				$excerpt = topica_excerpt(19,'');
				?>
				<p><?php  echo $excerpt ?></p>
			</div>
		</div>
	</div>
</article>
