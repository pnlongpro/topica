<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/11/2017
 * Time: 3:45 AM
 */
?>
<?php
$title             = strip_tags( get_the_title() );
$permalink         = get_permalink();
$site_name         = get_bloginfo( 'name' );
$excerpt           = get_the_excerpt();
$content           = get_the_content();
if ( $excerpt != "" ) {
	$excerpt = strip_tags( trim( preg_replace( '|\[(.+?)\](.+?\[/\\1\])?|s', '', $content ) ) );
	if ( function_exists( 'mb_strimwidth' ) ) {
		$excerpt = mb_strimwidth( $excerpt, 0, 100, '...' );
	}
}
?>
<div class="vlog-share-single">
	<a href="javascript:void(0);" class="vlog-facebook vlog-share-item" onclick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[url]=<?php echo esc_attr(urlencode(get_permalink()));?>','sharer', 'toolbar=0,status=0,width=620,height=280');" ><i class="fa fa-facebook"></i><span>Facebook</span></a>
	<a href="javascript:void(0);" class="vlog-twitter vlog-share-item" onclick="popUp=window.open('http://twitter.com/home?status=<?php echo esc_attr(urlencode(get_the_title())); ?> <?php echo esc_attr(urlencode(get_permalink())); ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;"><i class="fa fa-twitter"></i><span>Twitter</span></a>
	<a href="javascript:void(0);" class="vlog-reddit vlog-share-item" data-url="http://www.reddit.com/submit?url=<?php echo esc_attr(urlencode(get_permalink()));?>;title=<?php echo get_the_title(get_the_ID()) ?>"><i class="fa fa-reddit-alien"></i><span>Reddit</span></a>
	<a href="javascript:void(0);" class="vlog-pinterest vlog-share-item" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=<?php echo esc_attr(urlencode(get_permalink())); ?>&amp;description=<?php echo esc_attr(urlencode(get_the_title())); ?>&amp;media=<?php $arrImages = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); echo has_post_thumbnail() ? esc_attr($arrImages[0])  : "" ; ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;"><i class="fa fa-pinterest-p">
			</i><span>Pinterest</span>
	</a>
	<a href="mailto:?subject=<?php echo get_the_title(get_the_ID()) ?>;body=<?php echo esc_attr(urlencode(get_permalink())); ?>" class="vlog-mailto"><i class="fa fa-envelope-o"></i><span>Email</span></a>
</div>
