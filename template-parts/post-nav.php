<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/6/2015
 * Time: 5:16 PM
 */
// Don't print empty markup if there's nowhere to navigate.
$previous = (is_attachment()) ? get_post(get_post()->post_parent) : get_adjacent_post(false, '', true);
$next = get_adjacent_post(false, '', false);
if (!$next && !$previous) {
    return;
}
?>
<nav class="post-navigation" role="navigation">
    <div class="vlog-prev-next-cover-nav vlog-prev-next-nav">
        <?php
        previous_post_link('<div class="vlog-prev-link">%link</div>', _x('<i class="post-navigation-icon fa fa-chevron-left"></i> <div class="post-navigation-content"><div class="post-navigation-label">Bài trước</div> <div class="post-navigation-title">%title </div> </div> ', 'Previous post link', 'viettitan'));
        next_post_link('<div class="vlog-next-link">%link</div>', _x('<i class="post-navigation-icon fa fa-chevron-right"></i> <div class="post-navigation-content"><div class="post-navigation-label">Bài sau</div> <div class="post-navigation-title">%title</div></div> ', 'Next post link', 'viettitan'));
        ?>
    </div>
    <!-- .nav-links -->
</nav><!-- .navigation -->

