<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/11/2017
 * Time: 12:48 AM
 */
?>
<?php if( !is_single('post') ) : ?>
<div class="entry-meta">
	<div class="meta-item meta-date"><span class="updated meta-icon"><?php echo get_the_date('F j, Y'); ?></span></div>
	<?php if ( comments_open() || get_comments_number() ) : ?>
		<div class="meta-item meta-comments">
			<?php comments_popup_link(wp_kses_post(__('<span>Comment:</span> 0','topica')) ,wp_kses_post(__('<span>Comment:</span> 1','topica')),wp_kses_post(esc_html__('<span>Comment:</span> %','topica'))); ?>
		</div>
	<?php endif; ?>
</div>
<?php else : ?>
	<div class="entry-meta">
		<div class="meta-item meta-date">
			<span class="updated meta-icon"><?php echo get_the_date('F j, Y'); ?></span></div>
<!--		<div class="meta-item meta-views">748 views</div>-->
<!--		<div class="meta-item meta-rtime">1 min read</div>-->
	</div>
<?php endif; ?>
