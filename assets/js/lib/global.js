var $ = jQuery;
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

// Placeholder polyfill
$('input, textarea').placeholder();

// Info-boxes (touch toggle)
$('.touch .info-boxes').find(".box").on("click", function (e) {
	e.preventDefault();
	var $el = $(this);

	$el.parent().find(".box").not($el).removeClass("on");
	$el.toggleClass("on");
});

// Apply Now (touch toggle)
$('.touch .apply-now .link').on("click", function (e) {
		e.preventDefault();
		var $el = $(this);
		var $elWrapper = $el.find(".wrapper");

		$el.parent().find(".wrapper").not($elWrapper).removeClass("on");
		$el.find(".wrapper").toggleClass("on");
	})
	.find(".text").on("click", function () {
	var $el = $(this).closest("a");
	window.location = $el.prop("href");
});


$.localScroll({
	queue:true,
	duration:1000,
	hash:true
});

/**
 * Animate count
 *
 */
$(".countFade").css("opacity", 0);

$(".countUp").each( function () {
	$(this).waypoint(function(direction) {
		var $el = $(this);
		var comma = true;

		if (direction === "down") {
			$el.closest(".countFade").fadeTo(300, 1);
			if ($el.hasClass("countNoComma")) {
				comma = false;
			}
			$el.countUp({
				commaSep: comma,
				onComplete: function () {
					$el.waypoint("destroy");
				}
			});
		}
	}, {offset: "65%" });
});

/**
 * Scroll to top buttons
 *
 */
$("body").append('<a href="#" class="topButton scrollTop"><span class="icon-caret-up"><span class="sr-only">Scroll To Top</span></span></a>');
var $topButton = $('.topButton');

$(".scrollTop").on("click", function (e) {
	e.preventDefault();

	$('html, body').animate({scrollTop: 0}, 800);
});

/**
 * Animation Waypoints
 *
 */
$("[data-trigger-anim]").waypoint(function(direction) {
	$el = $(this);
	$el.find("." + $el.data("trigger-anim")).toggleClass("go");
	$el.waypoint("destroy");
}, {offset: "65%" });

/**
 * Sidebar Sticky Menu
 *
 */
var $sideBar = $(".side-bar");
if ($sideBar.length > 0) {
	var sWidth = $sideBar.width(),
		sHeight = $sideBar.height(),
		$sidebarWrapper = null,
		$W = $(window),
		$container = null,
		offset = 34,
		leftOffset = null,
		stuck = false,
		init = false;

	$sidebarWrapper = $sideBar.wrap('<div class="sidebarWrapper col-sm-3"></div>').closest('.sidebarWrapper');
	$container = $sidebarWrapper.closest('.container');

	// Set dimensions
	$sidebarWrapper.height($container.height());
	$sideBar.height(sHeight).width(sWidth);

	leftOffset = $sideBar.offset().left;

	$sideBar.waypoint(function (direction) {
		if (direction === 'down')
		{
			init = true;
			$sideBar.addClass("fixedPositionTop").css("left", "auto");
			stuck = true;
		}

		else
		{
			$sideBar.removeClass("fixedPositionTop").css("left", "auto");
		}
	}, {offset: offset, scrollThrottle: 55});

	var sidebarY = 0, sidebarContainerY = 0, sidebarDiffBtm, sidebarDiffTop;


	$W.on("scroll", function () {
			if (init) {
				sidebarY = $sideBar.offset().top + sHeight;
				sidebarContainerY = $container.offset().top + $container.height();
				sidebarDiffBtm = sidebarContainerY - (sidebarY + offset);
				sidebarDiffTop = $W.scrollTop() - (sidebarY - sHeight - offset);

				if (stuck && sidebarDiffBtm < 1) {
					$sideBar.addClass("absolutePositionBtm").css("left", "auto");
					stuck = false;
				} else if (!stuck && sidebarDiffTop < 1) {
					$sideBar.removeClass("absolutePositionBtm").css("left", leftOffset);
					stuck = true;
				}
			}
		})
		.on("resize", function () {
			init = false;
			$sideBar.removeClass("fixedPositionTop absolutePositionBtm").css({width: 'auto', height: 'auto'});
			$sidebarWrapper.css({height: 'auto'});
			$sidebarWrapper.height($container.height());
			$sideBar.height($sideBar.height()).width($sideBar.width());
		});

}

/**
 * Stop repainting header bg
 *
 */
$fixedElements = $(".header-bg, .big-video-wrapper");
$(".header").next().waypoint(function(direction) {
	$fixedElements.toggleClass("absolutePos");
}, {offset: 0});

/**
 * Sticky Local Navigation
 *
 */
var $localNav = $('.local-nav'),
	$scrollPoints = $localNav.find("a"),
	scrollIds = "",
	numScrollPoints = $scrollPoints.length - 1,
	$scrollItems;

$.each($scrollPoints, function (i) {
	if (i < numScrollPoints) {
		scrollIds = scrollIds + $(this).attr("href") + ", ";
	} else {
		scrollIds = scrollIds + $(this).attr("href");
	}
});

$scrollItems = $(scrollIds);

$scrollItems.waypoint(function (direction) {
	var location;
	var $localNav = $(".local-nav");
	var $el = $(this);
	if ($el.prop("id") != "sectioncarousel") {
		if (direction == "down") {
			location = $el.prop("id");
			$localNav.find("a").removeClass("current");
			$localNav.find("a[href='#" + location + "']").addClass("current");
		} else if (direction == "up") {
			$el = $(this).prev($scrollItems);
			location = $el.prop("id");
			$localNav.find("a").removeClass("current");
			$localNav.find("a[href='#" + location + "']").addClass("current");
		}
	}
}, { offset: 48 });

$('.local-nav-wrapper').waypoint(function() {
	$(".local-nav").find("a").removeClass("current");
});

$('.hide-sticky').waypoint(function(direction) {
	$localNav = $(".fixedPosition");

	if(direction == "down") {
		$topButton.toggleClass("fadeIn");
		$localNav.addClass("hide");
	} else if (direction == "up") {
		$topButton.toggleClass("fadeIn");
		$localNav.removeClass("hide");
	}
}, {offset: 48});

function setLocalNavHeight () {
	$('.local-nav-wrapper').height(49);
}

function bindLocalNav () {
	//$('.local-nav-wrapper').css("min-height", "49px");
	//$(".local-nav-wrapper nav").stalker({
	//	stalkerStyle: "fixedPosition",
	//	startCallback: function () {
	//		if(videoAPI !== null) {
	//			//videoAPI.pause();
	//		}
	//		$(".local-nav").find("a:first").addClass("current");
	//	},
	//	stopCallback: function () {
	//		if(videoAPI !== null) {
	//			//videoAPI.play();
	//		}
	//		$(".local-nav").find("a").removeClass("current");
	//	}
	//});
	var topSticky = 0,
		$adminBar = $('#wpadminbar');

	//if (($adminBar.length > 0) && ($adminBar.css('position') =='fixed')) {
		topSticky = $('#wpadminbar').height();
	//}
	console.log(topSticky);
	//$(".local-nav-wrapper").unstick();

	//var topSpacing = topSticky;
	$(".local-nav-wrapper").sticky({topSpacing:topSticky,topSticky: 32});
}


/**
 * Search Box
 *
 */
//var $searchBtn = $(".largeSearchBtn").hide();

$(".big-search").on("keyup keydown", function() {
	$el = $(this);
	$searchBtn = $el.parent().find(".largeSearchBtn");
	if($el.val()) {
		$searchBtn.fadeIn(250);
	} else {
		$searchBtn.fadeOut(200);
	}
});


/**
 * PhotoBox (Homepage)
 *
 */
function PhotoBox() {
	// Store sections by name
	this.photos = [];
	// Store section names in order
	this.photoKeys = [];

	this.numSections = 4;

	this.staggerDelay = 350;
	this.timeouts = [];
	this.zindex = 1;

	// jQuery Selectors
	this.$photoBox = $("#photoBox");
	this.$photoSections = this.$photoBox.children("li");
	this.$photoBoxItems = this.$photoBox.find("> li a");
}

// Add photos to a section
PhotoBox.prototype.setPhotos = function (section, photos) {
	if(section && photos && (photos.length > 0)) {
		this.photos[section] = photos;
		this.photoKeys.push(section);

		// Preload first photo
		var parentPhotoSrc = this.photos[section].shift();
		$("li[data-key='" + section + "']").find("a > img").attr("src", parentPhotoSrc);

		// Preload and position section photos
		this._preloadSection(section);
		this._positionSectionPhotos(section);

		// If all sections are loaded
		if(this.photoKeys.length == this.numSections) {

			this._loadBinds();
		}
	} else {
		//console.error("Invalid params for PhotoBox.setPhotos");
	}
};

// Preload section photos
PhotoBox.prototype._preloadSection = function (section) {
	this._preloadArray(this.photos[section]);
};

// Preload array of images
PhotoBox.prototype._preloadArray = function (arrayOfImages) {
	$(arrayOfImages).each(function() {
		$('<img/>')[0].src = this;
	});
};

// Load jQuery event binds
PhotoBox.prototype._loadBinds = function () {
	$("#photoBox .overlay-text").hide();

	var self = this;
	this.$photoBox.find("a").hover(debounce(function () {

		$el = $(this).parent();
		$el.parent().find(".overlay-text").hide();
		var section = $el.data("key");

		$el.find(".overlay-text").fadeIn(100);
		self._hideChildren($el.find(".overlay-photo img"));

		var $secPhotos = self.$photoSections.not($el).find("[data-section-key='" + section + "']");

		self._moveElemTop($secPhotos);

		self._staggeredAnimation($secPhotos);

	}, 150, false), function () {
		$el = $(this).parent();
		self._clearAnimations();

		$el.find(".overlay-text").hide();
	});

	this.$photoBox.parent().on("mouseleave", function () {
		var $el = $(this);

		self._clearAnimations();

		$el.find(".overlay-photo img").fadeOut(200);
		$el.find(".overlay-text").hide();
	});
};

PhotoBox.prototype._clearAnimations = function () {
	for(var i = 0, z = this.timeouts.length; i < z; i++) {
		clearTimeout(this.timeouts[i]);
	}

	this.timeouts = [];
};

// Hide all children
PhotoBox.prototype._hideChildren = function ($el) {
	$el.each(function () {
		$(this).fadeOut(300);
	});
};

// Move selected element to top
PhotoBox.prototype._moveElemTop = function ($el) {
	$el.each(function () {
		var $el = $(this);
		var parent = $el.parent();
		var elem = $el.detach().hide();

		elem.appendTo(parent);
	});
};

PhotoBox.prototype._staggeredAnimation = function($el) {
	var self = this;

	$el.each(function (i) {
		var $photo = $(this);

		self.timeouts.push(
			setTimeout(function () {
					$photo.fadeIn(500);
				},
				self.staggerDelay * i)
		);
	});
};

PhotoBox.prototype._positionSectionPhotos = function (section) {
	var sectionPhotos = this.photos[section];
	var $section = this.$photoBox.find("[data-key='" + section + "']");
	var self = this;

	this.$photoSections.not($section).each(function () {
		var $img = $("<img/>").attr("src", sectionPhotos.shift())
			.attr("data-section-key", section)
			.attr("alt", " ")
			.hide();

		$(this).find(".overlay-photo").append($img);
	});

	this._reloadSelectors();
};

PhotoBox.prototype._reloadSelectors = function () {
	this.$photoBox = $("#photoBox");
	this.$photoSections = this.$photoBox.children("li");
	this.$photoBoxItems = this.$photoBox.find("> li a");
};

function PhotoGrid () {
	this.currentPanel = 1;

	this._initVars();

	//this._padPanels();

	this._loadBinds();


	// if only two panels make sure to clone the 2nd panel
	if (this.numPanels === 2) {
		this.$panels.last().clone().appendTo(this.$panels.parent());
		this.$panels = this.$wrapper.children("li");
	}

	this._marginSet();
	this._wrapPanels();

	this._responsiveReload();

}

PhotoGrid.prototype._initVars = function () {
	this.$wrapper = $(".grid-carousel");
	this.$panels = this.$wrapper.children("li");
	this.$focused = this.$panels.filter(":first");

	this.numPanels = this.$panels.length;

	if (this.numPanels > 1) {
		this.$carouselNav = $(".carousel-nav").fadeIn();
	}

	var i = 0;
	this.$panels.each(function () {
		$(this).attr("data-i", i++);
	});

	this.panelHeight = this.$focused.height();
	this.panelWidth = this.$focused.width();

	this.$wrapper.height(this.panelHeight);
};

PhotoGrid.prototype._responsiveReload = function () {
	var self = this;
	$(window).on("resize", function () {
		self._initVars();
	});
};

PhotoGrid.prototype._loadBinds = function () {
	var self = this;

	$(".carousel-nav-left").on("click", function (e) {
		e.preventDefault();
		self._prev();
	});

	$(".carousel-nav-right").on("click", function (e) {
		e.preventDefault();
		self._next();
	});
};

PhotoGrid.prototype._prev = function () {
	this.$focused = this.$focused.prev();
	this.$panels = this.$wrapper.children("li");


	this._wrapPanels();
	this.$panels.animate({"marginLeft": "-=" + this.panelWidth}, 500);
};

PhotoGrid.prototype._next = function () {
	this.$focused = this.$focused.next();
	this.$panels = this.$wrapper.children("li");

	this._wrapPanels();
	this.$panels.animate({"marginLeft": "-=" + this.panelWidth}, 500);
};

PhotoGrid.prototype._marginSet = function () {
	var marginOffset = 0;
	var self = this;

	this.$panels.each(function () {
		var $el = $(this);
		$el.css("margin-left", marginOffset);

		marginOffset += self.panelWidth;
	});
};

PhotoGrid.prototype._wrapPanels = function () {
	this.$panels = this.$wrapper.children("li");
	var index = this.$wrapper.find("> li").index(this.$focused);
	var $first, $last, margin = 0;

	if (index <= 2 || index >= this.numPanels - 2) {
		$first = this.$panels.first();
		$last = this.$panels.last();
	}
	if (index <= 2) {
		margin = parseInt($first.css("margin-left"), null);
		$last.prependTo(this.$panels.parent()).css("margin-left", margin - this.panelWidth);
	} else if (index >= this.numPanels - 2) {
		margin = parseInt($last.css("margin-left"), null);
		$first.appendTo(this.$panels.parent()).css("margin-left", margin + this.panelWidth);
	}
};

// Mobile nav push menu
$("#menu-btn, .main-overlay").on("click", function (e) {

	e.preventDefault();
	togglePushNav();
	window.scrollTo(0,0);
	$(this).blur();
});

$mpDropdown = $(".mp-dropdown");
$(".toggle-mp-dropdown").on("click", function (e) {
	e.preventDefault();
	$mpDropdown.toggleClass('on');
});

// Mobile nav search
$("#menu-search-btn").on("click", function (e) {
	e.preventDefault();
	var $icon = $(this).find("span"),
		$mSearch = $(".mobile-search-wrapper");

	if ($icon.hasClass('icon-search')) {
		$mSearch.addClass('on').find(".search-box").focus();
		$icon.removeClass('icon-search').addClass('icon-caret-up');
	} else {
		$mSearch.removeClass('on').find(".search-box").blur();
		$icon.removeClass('icon-caret-up').addClass('icon-search');
	}
});

// Push Navigation
var $pushNav = $(".push-nav"),
	$mobileNav = $('.mobile-nav');

$pushNav.find(".mp-back-btn").on("click", function (e) {
	e.preventDefault();
	$(this).closest(".mp-level").removeClass("mp-push");
});
$pushNav.find("li > a").on("click", function (e) {
	if ($(this).parent().find("ul > li").length > 0) {
		e.preventDefault();
		$(this).next().addClass("mp-push");
	}
});

function togglePushNav () {
	if ($('.main-wrapper').hasClass('on')) {
		pushNavOff();
		$mpDropdown.removeClass('on');
	}
	else {
		pushNavOn();
	}
}

function pushNavOn () {
	var $curr;
	if ($curr = $pushNav.find(".current > a")) {
		$curr.trigger("click");
	}
	$('.main-wrapper').addClass('on');
	$('.push-nav').addClass('on');
}

function pushNavOff () {
	$('.main-wrapper').removeClass('on');
}

// Resize to cover while maintaining original aspect ratio
function resizeToCover (sourceWidth, sourceHeight, targetWidth, targetHeight) {
	var sourceAspect = sourceWidth / sourceHeight;
	var targetAspect = targetWidth / targetHeight;
	var newWidth = 0,
		newHeight = 0,
		diffWidth = 0,
		diffHeight = 0;

	diffWidth = 0;
	diffHeight = 0;

	if (sourceWidth < targetWidth || sourceHeight < targetHeight) {
		// Scale up to cover target size
		if (sourceAspect > targetAspect) {
			// Source has wider aspect ratio
			newHeight = targetHeight;
			newWidth =  targetHeight * targetAspect;
			diffWidth = ((newWidth - sourceWidth) / 2);
		} else {
			// Source has taller aspect ratio
			newWidth =  targetWidth;
			newHeight = targetWidth / sourceAspect;
			diffHeight = ((sourceHeight - newHeight) / 2);
		}
	} else {
		// Scale down to cover target size
		if (sourceAspect > targetAspect) {
			// Source has wider aspect ratio
			newHeight = targetHeight;
			newWidth =  targetHeight * sourceAspect;
			diffWidth = ((targetWidth - newWidth) / 2);
		} else {
			// Source has taller aspect ratio
			newHeight =  targetWidth / sourceAspect;
			newWidth = targetWidth;
			diffHeight = ((targetHeight - newHeight) / 2);
		}
	}

	return {
		width: newWidth,
		height: newHeight,
		offsetWidth: diffWidth,
		offsetHeight: diffHeight
	};
}

/**
 * Alert boxes
 */
$(".alert-close").on("click", function (e) {
	e.preventDefault();

	var $el = $(this);
	var $alert = $el.closest(".alert");
	var top = $alert.height() / 2;

	$el.css("top", top);

	if (Modernizr.csstransitions) {
		$alert.addClass("hide");
	} else {
		$alert.slideUp();
	}
});

$.localScroll.hash({
	queue:true,
	duration:1500
});