$(function($) {
	// Debug flag - set this to true to send console logs instead of dataLayer pushes
	var debugMode = false;
	// Default time delay before checking location
	var callBackTime = 100;
	// # px before tracking a reader
	var readerLocation = 200;
	// Set some flags for tracking & execution
	var timer = 0;
	var scroller = false;
	var didComplete = false;

	// Set some time variables to calculate reading time
	var startTime = new Date();
	var beginning = startTime.getTime();
	var totalTime = 0;

	var totalheight = $('body').height()
	var hit25 = hit50 = hit75 =  hit100 = false		//these will make sure the events only fire once
	var y100 = totalheight		//end of page
	var y25 = ((y100)*.25)		//breakpoint 25%
	var y50 = ((y100)*.5)		//breakpoint 50%
	var y75 = ((y100)*.75)		//breakpoint 75%

// Check the location and track user
	function trackLocation() {
		bottom = $(window).height() + $(window).scrollTop();

		// If user starts to scroll send an event
		if (bottom > readerLocation && !scroller) {
			currentTime = new Date();
			scrollStart = currentTime.getTime();
			timeToScroll = Math.round((scrollStart - beginning) / 1000);
			if (!debugMode) {
				dataLayer.push({'event':'scrollEvent',
					'scrollProgress':'start-scroll',
					'scrollTime':timeToScroll});
			} else {
				console.log('started scrolling ' + timeToScroll);
			}
			scroller = true;
		}
		// If user has hit 25%
		if (bottom >= y25 && hit25 == false) {
			currentTime = new Date();
			contentScrollEnd = currentTime.getTime();
			timeToContentEnd = Math.round((contentScrollEnd - scrollStart) / 1000);
			if (!debugMode) {
				dataLayer.push({'event':'scrollEvent',
					'scrollProgress':'25%',
					'scrollTime':timeToContentEnd});
			} else {
				console.log('hit 25% '+ timeToContentEnd);
			}
			hit25 = true
		}
		// If user has hit 50%
		if (bottom >= y50 && hit50 == false) {
			currentTime = new Date();
			contentScrollEnd = currentTime.getTime();
			timeToContentEnd = Math.round((contentScrollEnd - scrollStart) / 1000);
			if (!debugMode) {
				dataLayer.push({'event':'scrollEvent',
					'scrollProgress':'50%',
					'scrollTime':timeToContentEnd});
			} else {
				console.log('hit 50% '+ timeToContentEnd);
			}
			hit50 = true
		}
		// If user has hit 75%
		if (bottom >= y75 && hit75 == false) {
			currentTime = new Date();
			contentScrollEnd = currentTime.getTime();
			timeToContentEnd = Math.round((contentScrollEnd - scrollStart) / 1000);
			if (!debugMode) {
				dataLayer.push({'event':'scrollEvent',
					'scrollProgress':'75%',
					'scrollTime':timeToContentEnd});
			} else {
				console.log('hit 75% '+ timeToContentEnd);
			}
			hit75 = true
		}
		// If user has hit 100%
		if (bottom >= y100 && hit100 == false) {
			currentTime = new Date();
			contentScrollEnd = currentTime.getTime();
			timeToContentEnd = Math.round((contentScrollEnd - scrollStart) / 1000);
			if (!debugMode) {
				dataLayer.push({'event':'scrollEvent',
					'scrollProgress':'100%',
					'scrollTime':timeToContentEnd});
			} else {
				console.log('hit 100% '+ timeToContentEnd);
			}
			hit100 = true
		}
	}
// Track the scrolling and track location
	$(window).scroll(function() {
		if (timer) {
			clearTimeout(timer);
		}
		// Use a buffer so we don't call trackLocation too often.
		timer = setTimeout(trackLocation, callBackTime);
	});
});