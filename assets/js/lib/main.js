// Fastclick
//FastClick.attach(document.body);

jQuery(document).ready(function () {
	bindLocalNav();
	impactPage();
	inputfile();


});

var videoAPI = null;
var $ = jQuery;
function inputfile() {
	var $input = $('#your-file'),
		$label	 = $input.next( 'label' ),
		labelVal = $label.html();

	$input.on('change', function (e) {
		var fileName = '';

		if (this.files && this.files.length > 1)
			fileName = ( this.getAttribute('data-multiple-caption') || '' ).replace('{count}', this.files.length);
		else if (e.target.value)
			fileName = e.target.value.split('\\').pop();

		if (fileName)
			$label.find('span').html(fileName);
		else
			$label.html(labelVal);
	});

	// Firefox bug fix
	$input
		.on('focus', function () {
			$input.addClass('has-focus');
		})
		.on('blur', function () {
			$input.removeClass('has-focus');
		});
}
function homePage(func) {
	// Default variables for video background
	var windowWidth = $(window).width(),
		mobileBreakpoint = 768,
		pb = null;

	// Responsive
	$(window).on('resize', function () {
		var $header = $('.header');
		windowWidth = $(window).width();

		if (windowWidth > mobileBreakpoint) {
			// Desktop
			$('.grid-carousel-wrap').flexslider({
				slideshow   : false,
				animation   : "slide",
				minSlides   : 1,
				maxSlides   : 1,
				margin      : 0,
				move        : 1,
				controlNav  : false,
				directionNav: false,
				prevText    : "<span class='sr-only'>Next Slide</span>",
				nextText    : "<span class='sr-only'>Next Slide</span>",
				start       : function (slider) {
					$('.carousel-nav-left').on("click", function (e) {
						e.preventDefault();
						slider.flexAnimate(slider.getTarget("prev"));
					});
					$('.carousel-nav-right').on("click", function (e) {
						e.preventDefault();
						slider.flexAnimate(slider.getTarget("next"));
					});
				}
			});

			// Run callback function
			if (typeof func == 'function') {
				func();
			}

		} else {
			if (videoAPI !== null) {
				videoAPI.pause();
			}

			// Mobile Flexslider
			$('.mobile-slider').flexslider({
				slideshow   : false,
				animation   : "slide",
				width       : 220,
				minSlides   : 1,
				maxSlides   : 3,
				margin      : 1,
				move        : 1,
				controlNav  : false,
				directionNav: true,
				prevText    : "<span class='sr-only'>Next Slide</span>",
				nextText    : "<span class='sr-only'>Next Slide</span>",

			});
		}
	}).resize();
}

function aboutPage() {
	$('.full-slider').flexslider({
		slideshow   : false,
		animation   : "slide",
		minSlides   : 1,
		maxSlides   : 1,
		margin      : 0,
		move        : 1,
		controlNav  : false,
		directionNav: true,
		prevText    : "<span class='sr-only'>Previous Slide</span>",
		nextText    : "<span class='sr-only'>Next Slide</span>",

	});
	$('.history-slider').flexslider({
		slideshow   : false,
		animation   : "slide",
		minSlides   : 1,
		maxSlides   : 1,
		margin      : 0,
		move        : 1,
		controlNav  : true,
		directionNav: true,
		prevText    : "<span class='icon-chevron-left'><span class='sr-only'>Previous Slide</span></span>",
		nextText    : "<span class='icon-chevron-right'><span class='sr-only'>Next Slide</span></span>",
	});
}


function studentLifePage() {
	// ********** TESTING DATA *********** //
	pb = new PhotoBox();

	pb.setPhotos("campusLiving", [
		"assets/main/img/student-life/quadrant/livingmain.jpg",
		"assets/main/img/student-life/quadrant/living1.jpg",
		"assets/main/img/student-life/quadrant/living2.jpg",
		"assets/main/img/student-life/quadrant/living3.jpg"
	]);

	pb.setPhotos("campusRecreation", [
		"assets/main/img/student-life/quadrant/recmain.jpg",
		"assets/main/img/student-life/quadrant/rec2.jpg",
		"assets/main/img/student-life/quadrant/rec3.jpg",
		"assets/main/img/student-life/quadrant/rec4.jpg"
	]);

	pb.setPhotos("food", [
		"assets/main/img/student-life/quadrant/foodmain.jpg",
		"assets/main/img/student-life/quadrant/food1.jpg",
		"assets/main/img/student-life/quadrant/food2.jpg",
		"assets/main/img/student-life/quadrant/food3.jpg"
	]);

	pb.setPhotos("healthSafety", [
		"assets/main/img/student-life/quadrant/safetymain.jpg",
		"assets/main/img/student-life/quadrant/safety2.jpg",
		"assets/main/img/student-life/quadrant/safety3.jpg",
		"assets/main/img/student-life/quadrant/safety4.jpg"
	]);
}


function researchPage() {
	$('.research-slider').flexslider({
		slideshow   : false,
		animation   : "slide",
		minSlides   : 1,
		maxSlides   : 1,
		margin      : 0,
		move        : 1,
		controlNav  : true,
		directionNav: true,
		prevText    : "<span class='icon-chevron-left'><span class='sr-only'>Previous Slide</span></span>",
		nextText    : "<span class='icon-chevron-right'><span class='sr-only'>Next Slide</span></span>"
	});
}

function impactPage() {

	$('.mobile-slider').flexslider({
		slideshow   : false,
		animation   : "slide",
		width       : 220,
		minSlides   : 1,
		maxSlides   : 3,
		margin      : 1,
		move        : 1,
		controlNav  : false,
		directionNav: true,
		prevText    : "<span class='sr-only'>Previous Slide</span>",
		nextText    : "<span class='sr-only'>Next Slide</span>",

	});
}

function setAlertCookie(alertID) {
	alertID = typeof alertID !== 'undefinded' ? alertID : "x12";

	if (document.cookie.indexOf("UNRAlerts") != -1) {
		var alertCookie = getAlertCookie().concat(",", alertID);
		document.cookie = "UNRAlerts=" + alertCookie;
	}
	else {//Create New Cookie
		document.cookie = "UNRAlerts=" + alertID + "; ";
	}
}

function getAlertCookie() {
	var ca = document.cookie.split(';');

	for (var i = 0; i < ca.length; i++) {
		var c = ca[i].trim();
		if (c.indexOf("UNRAlerts") == 0) return c.substring(10, c.length);
	}
	return "";
}

function findAlertCookie(alertID) {
	var cookieValues = getAlertCookie();
	var cookieList = cookieValues.split(",");

	for (var i = 0; i < cookieList.length; i++) {
		var c = cookieList[i].trim();
		if (c.indexOf("alertID") == 0) return true;
	}
	return false;
}

function setAlertSessionStorage(alertID) {
	if (window.sessionStorage) {
		if (sessionStorage.getItem("UNRAlerts") == null) { //Create Session var
			sessionStorage.setItem("UNRAlerts", alertID)
		}
		else { //Append current session
			var alertSession = sessionStorage.getItem("UNRAlerts").concat(",", alertID);
			sessionStorage.setItem("UNRAlerts", alertSession);
		}
	}
}

function getAlertSessionStorage(alertID) {
	if (window.sessionStorage) {
		return sessionStorage.getItem("UNRAlerts");
	}
}

/* ==========================================================
 * bootstrap-carousel.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#carousel
 * ==========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

	"use strict"; // jshint ;_;


	/* CAROUSEL CLASS DEFINITION
	 * ========================= */

	var Carousel = function (element, options) {
		this.$element = $(element)
		this.$indicators = this.$element.find('.carousel-indicators')
		this.options = options
		this.options.pause == 'hover' && this.$element
			.on('mouseenter', $.proxy(this.pause, this))
			.on('mouseleave', $.proxy(this.cycle, this))
	}

	Carousel.prototype = {

		cycle: function (e) {
			if (!e) this.paused = false
			if (this.interval) clearInterval(this.interval);
			this.options.interval
			&& !this.paused
			&& (this.interval = setInterval($.proxy(this.next, this), this.options.interval))
			return this
		}

		, getActiveIndex: function () {
			this.$active = this.$element.find('.item.active')
			this.$items = this.$active.parent().children()
			return this.$items.index(this.$active)
		}

		, to: function (pos) {
			var activeIndex = this.getActiveIndex()
				, that = this

			if (pos > (this.$items.length - 1) || pos < 0) return

			if (this.sliding) {
				return this.$element.one('slid', function () {
					that.to(pos)
				})
			}

			if (activeIndex == pos) {
				return this.pause().cycle()
			}

			return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
		}

		, pause: function (e) {
			if (!e) this.paused = true
			if (this.$element.find('.next, .prev').length && $.support.transition.end) {
				this.$element.trigger($.support.transition.end)
				this.cycle(true)
			}
			clearInterval(this.interval)
			this.interval = null
			return this
		}

		, next: function () {
			if (this.sliding) return
			return this.slide('next')
		}

		, prev: function () {
			if (this.sliding) return
			return this.slide('prev')
		}

		, slide: function (type, next) {
			var $active = this.$element.find('.item.active')
				, $next = next || $active[type]()
				, isCycling = this.interval
				, direction = type == 'next' ? 'left' : 'right'
				, fallback = type == 'next' ? 'first' : 'last'
				, that = this
				, e

			this.sliding = true

			isCycling && this.pause()

			$next = $next.length ? $next : this.$element.find('.item')[fallback]()

			e = $.Event('slide', {
				relatedTarget: $next[0]
				, direction  : direction
			})

			if ($next.hasClass('active')) return

			if (this.$indicators.length) {
				this.$indicators.find('.active').removeClass('active')
				this.$element.one('slid', function () {
					var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
					$nextIndicator && $nextIndicator.addClass('active')
				})
			}

			if ($.support.transition && this.$element.hasClass('slide')) {
				this.$element.trigger(e)
				if (e.isDefaultPrevented()) return
				$next.addClass(type)
				$next[0].offsetWidth // force reflow
				$active.addClass(direction)
				$next.addClass(direction)
				this.$element.one($.support.transition.end, function () {
					$next.removeClass([type, direction].join(' ')).addClass('active')
					$active.removeClass(['active', direction].join(' '))
					that.sliding = false
					setTimeout(function () {
						that.$element.trigger('slid')
					}, 0)
				})
			} else {
				this.$element.trigger(e)
				if (e.isDefaultPrevented()) return
				$active.removeClass('active')
				$next.addClass('active')
				this.sliding = false
				this.$element.trigger('slid')
			}

			isCycling && this.cycle()

			return this
		}

	}


	/* CAROUSEL PLUGIN DEFINITION
	 * ========================== */

	var old = $.fn.carousel

	$.fn.carousel = function (option) {
		return this.each(function () {
			var $this = $(this)
				, data = $this.data('carousel')
				, options = $.extend({}, $.fn.carousel.defaults, typeof option == 'object' && option)
				, action = typeof option == 'string' ? option : options.slide
			if (!data) $this.data('carousel', (data = new Carousel(this, options)))
			if (typeof option == 'number') data.to(option)
			else if (action) data[action]()
			else if (options.interval) data.pause().cycle()
		})
	}

	$.fn.carousel.defaults = {
		interval: 5000
		, pause : 'hover'
	}

	$.fn.carousel.Constructor = Carousel


	/* CAROUSEL NO CONFLICT
	 * ==================== */

	$.fn.carousel.noConflict = function () {
		$.fn.carousel = old
		return this
	}

	/* CAROUSEL DATA-API
	 * ================= */

	$(document).on('click.carousel.data-api', '[data-slide], [data-slide-to]', function (e) {
		var $this = $(this), href
			, $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
			, options = $.extend({}, $target.data(), $this.data())
			, slideIndex

		$target.carousel(options)

		if (slideIndex = $this.attr('data-slide-to')) {
			$target.data('carousel').pause().to(slideIndex).cycle()
		}

		e.preventDefault()
	})

}(window.jQuery);

/* ========================================================
 * bootstrap-tabs.js v1.3.0
 * http://twitter.github.com/bootstrap/javascript.html#tabs
 * ========================================================
 * Copyright 2011 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================== */


(function ($) {

	function activate(element, container) {
		container.find('.active').removeClass('active')
		element.addClass('active')
	}

	function tab(e) {
		var $this = $(this)
			, href = $this.attr('href')
			, $ul = $(e.liveFired)
			, $controlled

		if (/^#\w+/.test(href)) {
			e.preventDefault()

			if ($this.hasClass('active')) {
				return
			}

			$href = $(href)

			activate($this.parent('li'), $ul)
			activate($href, $href.parent())
		}
	}


	/* TABS/PILLS PLUGIN DEFINITION
	 * ============================ */

	$.fn.tabs = $.fn.pills = function (selector) {
		return this.each(function () {
			$(this).delegate(selector || '.tabs li > a, .pills > li > a', 'click', tab)
		})
	}

	$(document).ready(function () {
		$('body').tabs('ul[data-tabs] li > a, ul[data-pills] > li > a')
	})

})(window.jQuery || window.ender)