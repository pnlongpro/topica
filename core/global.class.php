<?php
//==============================================================================
// GLOBAL VARIABLE
//==============================================================================
if (class_exists('Topica_Global')) {
	return;
}
class Topica_Global {
	// GET OPTION KEY VALUE
	public static function get_option($key, $default = null) {
		if (isset($GLOBALS['topica_options']) && isset($GLOBALS['topica_options'][$key])) {
			return $GLOBALS['topica_options'][$key];
		}
		return $default;
	}

	public static function &get_options() {
		if (isset($GLOBALS['topica_options']) && is_array($GLOBALS['topica_options'])) {
			return $GLOBALS['topica_options'];
		}
		$GLOBALS['topica_options'] = array();
		return $GLOBALS['topica_options'];
	}


	// GET meta_boxs
	public static function &get_meta_boxes() {
		if (isset($GLOBALS['topica_meta_boxes']) && is_array($GLOBALS['topica_meta_boxes'])) {
			return $GLOBALS['topica_meta_boxes'];
		}
		$GLOBALS['topica_meta_boxes'] = array();
		return $GLOBALS['topica_meta_boxes'];
	}
	public static function &get_image_size() {
		if (isset($GLOBALS['topica_image_size']) && is_array($GLOBALS['topica_image_size'])) {
			return $GLOBALS['topica_image_size'];
		}
		$GLOBALS['topica_image_size'] = array(
			'blog-large-image-full-width' => array(
				'width' => 1170,
				'height' => 658
			),
			'blog-large-image-sidebar' => array(
				'width' => 870,
				'height' => 490
			),
			'blog-medium-image' => array(
				'width' => 570,
				'height' => 321
			),
			'blog-related' => array(
				'width' => 370,
				'height' => 248
			),
			'blog-sidebar' => array(
				'width' => 76,
				'height' => 58
			),
			'post-large' => array(
				'width' => 570,
				'height' => 380
			),
			'post-small' => array(
				'width' => 270,
				'height' => 181
			),
		);
		return $GLOBALS['topica_image_size'];
	}
}