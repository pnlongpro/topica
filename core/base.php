<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/2/2017
 * Time: 7:20 PM
 */
/*================================================
GET POST META
================================================== */
if ( !function_exists( 'topica_get_post_meta' ) ) {
	function topica_get_post_meta( $id, $key = "", $single = false ) {

		$GLOBALS['topica_post_meta'] = isset( $GLOBALS['topica_post_meta'] ) ? $GLOBALS['topica_post_meta'] : array();
		if ( ! isset( $id ) ) {
			return;
		}
		if ( ! is_array( $id ) ) {
			if ( ! isset( $GLOBALS['topica_post_meta'][ $id ] ) ) {
				//$GLOBALS['topica_post_meta'][ $id ] = array();
				$GLOBALS['topica_post_meta'][ $id ] = get_post_meta( $id );
			}
			if ( ! empty( $key ) && isset( $GLOBALS['topica_post_meta'][ $id ][ $key ] ) && ! empty( $GLOBALS['topica_post_meta'][ $id ][ $key ] ) ) {
				if ( $single ) {
					return maybe_unserialize( $GLOBALS['topica_post_meta'][ $id ][ $key ][0] );
				} else {
					return array_map( 'maybe_unserialize', $GLOBALS['topica_post_meta'][ $id ][ $key ] );
				}
			}

			if ( $single ) {
				return '';
			} else {
				return array();
			}

		}

		return get_post_meta( $id, $key, $single );
	}
}