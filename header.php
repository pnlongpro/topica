<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="cms-area" content="unr main" />
	<meta name="Data-CMS" content="mvc" />
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

	<!--[if lt IE 8]>
	<link rel="stylesheet" href="assets/css/ie7.css">
	<![endif]-->
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="assets/css/ie.css">
	<script src="assets/js/lib/respond.js"></script>
	<script src="assets/js/lib/respond.js"></script>
	<![endif]-->
	<!-- JAVASCRIPT ASSETS -->
	<?php wp_head(); ?>
</head>
<body <?php body_class() ?>>
<?php
	$prefix = 'topica_';
	$topica_options = &Topica_Global::get_options();
?>
<nav class="push-nav" style="display: none">
	<?php
	$arg_menu = array(
		'menu_id'         => 'mobile-menu',
		'container'       => false,
		'container_id'    => '',
		'container_class' => '',
		'theme_location'  => 'mobile-menu',
		'menu_class'      => 'mobile-menu',
	);
	wp_nav_menu( $arg_menu );
	?>
</nav>
<div class="main-wrapper">
	<div class="main-overlay" style="display: none">
	</div>
	<div class="alert hide">
		<div class="container">
			<div class="alert-wrapper"></div>
		</div>
	</div>
	<nav class="mobile-nav" style="display: none">
		<a title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" style="background-image: url(<?php echo $topica_options['logo']['url'] ?>)" href="<?php echo home_url() ?>" class="mobile-logo logo-swap"><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?></a>
		<a href="#" id="menu-btn" class="icon-btn"><span class="icon-menu"><span class="sr-only">Expand Navigation</span></span></a>
		<a href="#" id="menu-search-btn" class="icon-btn"><span class="icon-search"><span class="sr-only">Expand Search box</span></span></a>
		<div class="mobile-search-wrapper">
			<form class="mobile-search" method="get" action="<?php echo home_url(); ?>" role="search">
				<input id="i" name="s" type="text" title="Mobile Search" class="search-box" size="30" placeholder="Type a keyword..." >
				<button class="search-btn" type="submit">
					<span class="icon-search"><span class="sr-only">Search</span></span>
				</button>
				<div class="sitemap-link">
					<a href="<?php echo home_url() ?>/site-map">Xem cấu trúc Website tại đây</a>
				</div>
			</form>
		</div>
	</nav>
	<!-- HEADER -->
	<?php
		$post_id = get_the_ID();
		if( is_category() ){
			$show_banner = '1';
			$banner_type = '1';
			$banner_title = 'Tin tức';
			$banner_description = 'One of the nation\'s top public research universities— yet small enough to connect with professors who guide you to success';
			$image_url = 'http://wpdemo.dev/topica/wp-content/uploads/2016/12/10-1.jpg';
		}else{
			$show_banner = rwmb_meta( $prefix . 'show_hide_banner',$post_id );
			$banner_type = rwmb_meta( $prefix . 'header_banner_layout',$post_id );
			$banner_title = rwmb_meta( $prefix . 'header_banner_title',$post_id );
			$banner_description = rwmb_meta( $prefix . 'header_banner_description',$post_id );
			if ( $banner_type == '0' ) {
				$banner_item = rwmb_meta( $prefix . 'header_banner_video',$post_id );
			} else {
				$banner_item = rwmb_meta( $prefix . 'header_banner_image',$post_id );
			}
		}

	$header_class = ($show_banner == '0' || $show_banner == '') ? 'header002 small-header header no-hero' : 'header header-hero';

	?>
	<header class="<?php echo $header_class ?>">

		<nav class="navbar">
			<!-- PRIMARY NAVBAR -->
			<section class="primary-nav row" aria-label="Primary Navigation">
				<div class="container">
					<div class="col-sm-12">
						<?php
						$arg_menu = array(
							'menu_id'         => 'left-menu',
							'container'       => 'div',
							'container_id'    => 'left-nav',
							'container_class' => 'left-nav',
							'theme_location'  => 'header-menu-left',
							'menu_class'      => 'left-menu',
						);
						wp_nav_menu( $arg_menu );
						?>
						<div class="center-nav">
							<a title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" style="background-image: url(<?php echo $topica_options['logo']['url'] ?>)" href="<?php echo home_url() ?>" id="header-logo" class="logo logo-swap"><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?></a>
						</div>
						<?php
						$arg_menu = array(
							'menu_id'         => 'right-menu',
							'container'       => 'div',
							'container_id'    => 'right-nav',
							'container_class' => 'right-nav',
							'theme_location'  => 'header-menu-right',
							'menu_class'      => 'right-menu',
						);
						wp_nav_menu( $arg_menu );
						?>
					</div>
				</div>
			</section>
		</nav>
		<?php


		if ( $show_banner != '0' && $show_banner != '' ):
			?>
			<?php
			if ( $banner_type == 0 ) :
				?>
				<!-- VIDEO OVERLAY -->
				<div class="big-video-gradient-overlay"></div>
				<div class="big-video-wrapper">
					<!-- VIDEO BACKGROUND -->
					<!-- Vimeo -->
					<!-- <iframe src="//player.vimeo.com/video/83775773?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1" width="1600" height="900" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
					<div class="vimeo-video">
						<?php echo $banner_item; ?>
					</div>

				</div>
			<?php else : ?>
				<div class="header-bg-wrapper">
					<div class="header-bg-gradient"></div>
					<?php
						if( !is_category() ){
							$image_attr = wp_get_attachment_image_src($banner_item, 'full');
							$image_url = $image_attr[0];
						}
					?>
					<div class="header-bg" style="background-image: url(<?php echo $image_url ?>);"></div>
				</div>
			<?php endif; ?>
			<!-- HERO -->
			<div class="hero container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<?php if( $banner_title != '' ) : ?>
						<h1 id="unr"><?php echo esc_html($banner_title) ?></h1>
						<?php endif; ?>
					</div>
				</div>
				<div class="row">
					<div class="subheader col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
						<div class="subheader-inner" id="banner">
							<?php if( $banner_description != '' ) : ?>
								<?php echo $banner_description ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</header>