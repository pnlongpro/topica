<!-- search -->
<section class="search" aria-label="Search">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="row">
					<form class="form" method="get" action="<?php echo home_url(); ?>" role="search">
					<label for="big-search" class="search-label">
						Chúng tôi có thể hỗ trợ bạn tìm kiếm?</label>
					<div class="search-wrapper">
						<span class="overlay-icon icon-search"></span>
						<button class="largeSearchBtn search-btn" type="submit">
							<span class="icon-chevron-right"><span class="sr-only">Search</span></span>
						</button>
						<input name="s" id="big-search" type="text" title="Search the University of Nevada, Reno" placeholder="Bạn đang tìm kiếm....?" class="big-search search-large">
						<input type="hidden" name="post_type" value="chuyen-nganh" />
					</div>
					<div class="sitemap-link sm-hide"><a href="<?php echo home_url() ?>/site-map">Xem cấu trúc Website tại đây</a></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /search -->
