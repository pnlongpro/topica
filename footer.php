<!-- SEARCH -->
<?php
	$topica_options = &Topica_Global::get_options();
	$prefix             = 'topica_';
	$show_button       = rwmb_meta( $prefix . 'footer_show_hide_button' );
	$show_search       = rwmb_meta( $prefix . 'footer_show_hide_search' );

//	$show_button = $topica_options['show_button_panel'];
//	$show_search = $topica_options['show_search_panel'];

	if( $show_button == '' || $show_button == '-1' ){
		$show_button = $topica_options['show_button_panel'];
	}
	if( $show_search == '' || $show_search == '-1' ){
		$show_search = $topica_options['show_search_panel'];
	}
?>
<?php if( $show_button != '' && $show_button == 'show' ) : ?>
	<section class="next-step-cta hide-sticky" aria-label="Take The Next Step">
		<div class="container">
			<div class="row">
				<header class="col-sm-12">
					<h2>
						<span class="lbreak"><?php echo $topica_options['search_panel_title'] ?></span></h2>
				</header>
			</div>
			<div class="row">
				<ul class="col-sm-12">
					<li><a href="<?php echo $topica_options['search_panel_url_contact'] ?>" class="btn cta-btn">THÔNG TIN LIÊN HỆ</a></li>
					<li><a href="<?php echo $topica_options['search_panel_url_register'] ?>" class="btn cta-btn">ĐĂNG KÝ SỰ KIỆN</a> </li>
					<li><a href="<?php echo $topica_options['search_panel_url_support'] ?>" class="btn cta-btn btn-solid" data-track="event" data-cat="interaction" data-act="button footer" data-lab="apply" data-appintent="true">NHẬN TƯ VẤN</a> </li>
				</ul>
			</div>
			<div class="row">
				<ul class="col-sm-12 tel">
					<li><a href="javascript: var e = window.open('https://chat2.sightmaxondemand.com/unr/SightMaxAgentInterface/PreChatSurvey.aspx?accountID=9&amp;siteID=12&amp;queueID=12&amp;skipsurvey=false','chatWindow','width=490,height=420,resizable=0,scrollbars=no,menubar=no,status=no');" onmouseout="top.status=''; return true;" onmousedown="top.status='Chat with the Office for Prospective Students';return true;" onmouseover="top.status='Chat with the Office for Prospective Students';return true;">Live Chat</a></li>
					<li><?php echo $topica_options['search_panel_phone'] ?></li>
					<li><a href="mailto:<?php echo $topica_options['search_panel_email'] ?>"><?php echo $topica_options['search_panel_email'] ?></a></li>
				</ul>
			</div>
		</div>
	</section>
<?php endif; ?>
<?php if( $show_search != '' && $show_search == 'show' ) : ?>
	<?php echo get_search_form(); ?>
<?php endif; ?>
<!-- FOOTER -->

<footer class="main-footer">
	<div class="container">
		<div class="row">
			<?php
				for( $i=1; $i<=6;$i++ ){
					?>
						<div class="footer-links col-sm-2">
							<?php dynamic_sidebar( 'sidebar-footer-'.$i.'' ); ?>
						</div>
					<?php
				}
			?>

		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-logo-line">
					<div class="footer-logo">
						<a href="<?php echo home_url() ?>" class="logo-swap" id="footer-logo"><?php echo get_bloginfo('title') ?></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<ul class="footer-social col-sm-12">
				<?php
					$facebook_url = $topica_options['social_facebook'] != '' ? $topica_options['social_facebook'] : '#';
					$twitter_url = $topica_options['social_twitter'] != '' ? $topica_options['social_twitter'] : '#';
					$youtube_url = $topica_options['social_youtube'] != '' ? $topica_options['social_youtube'] : '#';
					$flickr_url = $topica_options['social_flickr'] != '' ? $topica_options['social_flickr'] : '#';
					$instagram_url = $topica_options['social_instagram'] != '' ? $topica_options['social_instagram'] : '#';
					$linkedin_url = $topica_options['social_linkedin'] != '' ? $topica_options['social_linkedin'] : '#';
				?>
				<li>
					<a href="<?php echo $facebook_url ?>">
						<img alt="University of Nevada, Reno Facebook" src="<?php echo THEME_URL ?>assets/img/facebook-square-icon.svg" />
						<span class="sr-only">Facebook</span>
					</a>
				</li>
				<li>
					<a href="<?php echo $twitter_url ?>">
						<img alt="University of Nevada, Reno Twitter" src="<?php echo THEME_URL ?>assets/img/twitter-icon.svg" />
						<span class="sr-only">Twitter</span>
					</a>
				</li>
				<li>
					<a href="<?php echo $youtube_url ?>">
						<img alt="University of Nevada, Reno YouTube" src="<?php echo THEME_URL ?>assets/img/youtubeblock-icon.svg" />
						<span class="sr-only">Youtube</span>
					</a>
				</li>
				<li>
					<a href="<?php echo $flickr_url ?>">
						<img alt="University of Nevada, Reno Flickr" src="<?php echo THEME_URL ?>assets/img/flickr-icon.svg" />
						<span class="sr-only">Flickr</span>
					</a>
				</li>
				<li>
					<a href="<?php echo $instagram_url ?>">
						<img alt="University of Nevada, Reno Instagram" src="<?php echo THEME_URL ?>assets/img/instagram-icon.svg" />
						<span class="sr-only">Instagram</span>
					</a>
				</li>
				<li>
					<a href="<?php echo $linkedin_url ?>">
						<img alt="University of Nevada, Reno Linkedin" src="<?php echo THEME_URL ?>assets/img/linkedin-icon.svg" />
						<span class="sr-only">Linked In</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="row">
			<ul class="footer-info col-sm-12">
				<li>University of Nevada, Reno</li>
				<span>|</span>
				<li>1664 N. Virginia Street, Reno 89557</li>
				<span>|</span>
				<li>(775) 784-1110</li>
			</ul>
		</div>
		<nav class="row">
			<ul class="footer-nav col-sm-12">
				<li>
					<a title="Website Help Form" href="http://www.formstack.com/forms/?1226969-V0nnGE7i8j&URL=http://www.unr.edu/">Website Help</a>
					<span>|</span></li>
				<li><a title="General Information: Contact Us" href="/general-information/contact-us">Contact Us</a>
					<span>|</span></li>
				<li><a title="General Information: Copyright" href="/general-information/copyright">Copyright</a>
					<span>|</span></li>
				<li><a title="General Information: Privacy" href="/general-information/privacy">Privacy</a>
					<span>|</span></li>
				<li>
					<a title="General Information: Accessibility" href="/general-information/accessibility">Accessibility</a>
					<span>|</span></li>
				<li>
					<a title="General Information: Emergency Information" href="/general-information/emergency">Emergency Information</a>
					<span>|</span></li>
				<li>
					<a title="Search for Employment and Careers" href="https://www.unrsearch.com/">Employment &amp Careers</a>
					<span>|</span></li>
				<li>
					<a title="Business Center North: Business with Us" href="http://www.bcn-nshe.org/purchasing/business">Doing Business with Us</a>
				</li>
			</ul>
		</nav>
	</div>
</footer>
</div>

<script type="text/javascript">
	$(function () {
		$('#iconDropdown1').click(function () {
			setTimeout(function () {
				$('#q').focus();
			}, 0);
		});
	});
</script>

<?php wp_footer(); ?>
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>
</body>

</html>