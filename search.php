<?php get_header(); ?>
<?php
$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array(
		'post_type' => 'chuyen-nganh',
		's' => $_GET['s'],
		'paged'             =>      $page
	);
	query_posts($args);
?>
<div class="vlog-section ">
	<div class="container">
		<h1></h1>
		<div class="vlog-content">
			<div class="row">
				<div class="vlog-module module-posts col-lg-12">
					<div class="vlog-mod-head">
						<div class="vlog-mod-title">
							<h4 style="font-family:verdana;"><?php echo sprintf( __( '%s Kết quả tìm kiếm cho  ', 'topica' ), $wp_query->found_posts ); echo '<strong>'.$_GET['s'].'</strong>' ?></h4></div>
					</div>
					<div class="row vlog-posts row-eq-height vlog-posts">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php
							$classed = array();
							$classed[] = 'vlog-lay-c vlog-post col-lg-4 col-md-4 col-sm-6 col-xs-12';
							?>
							<article <?php post_class($classed) ?>>
								<?php if( has_post_thumbnail() ) : ?>
									<div class="entry-image">
										<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
											<?php
											$thumbnail_url = '';
											$width = 366;
											$height = 205;
											$image = get_post_thumbnail_id( get_the_ID() );
											if (!empty($image)) {
												$images_attr = wp_get_attachment_image_src($image, "full");
												if (isset($images_attr)) {
													$resize = matthewruddy_image_resize($images_attr[0], $width, $height);
													if ($resize != null)
														$thumbnail_url = $resize['url'];
												}
											}
											?>
											<img src="<?php echo $thumbnail_url ?>" class="attachment-vlog-lay-b size-vlog-lay-b wp-post-image" alt="<?php the_title() ?>" >
									</div>
								<?php endif; ?>

								<div class="entry-header">
									<h2 class="entry-title h2">
										<a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
								</div>

								<?php get_template_part('template-parts/post','meta') ?>

								<div class="entry-content">
									<?php
									$content           = get_the_content();
									$excerpt = topica_excerpt(19,'');
									?>
									<p><?php  echo $excerpt ?></p>
								</div>

							</article>
						<?php endwhile; ?>
					</div>
					<div class="blog-paging">
						<?php echo topica_paging_nav(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
