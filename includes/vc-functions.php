<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/9/2017
 * Time: 4:38 AM
 */

function topica_add_vc_param()
{
	if (function_exists('vc_add_param')) {
		vc_add_param('vc_tta_accordion', array(
				'type' => 'dropdown',
				'param_name' => 'style',
				'value' => array(
					esc_html__('Icon backround dark, active gray', 'topica') => 'accordion_style1',
					esc_html__('Icon backround gray, active dark', 'topica') => 'accordion_style2',
					esc_html__('Panel title transparent, active transparent', 'topica') => 'accordion_style3',
					esc_html__('Panel title transparent, active dark', 'topica') => 'accordion_style4',
					esc_html__('Classic', 'topica') => 'classic',
					esc_html__('Modern', 'topica') => 'modern',
					esc_html__('Flat', 'topica') => 'flat',
					esc_html__('Outline', 'topica') => 'outline',
				),
				'heading' => esc_html__('Style', 'topica'),
				'description' => esc_html__('Select accordion display style.', 'topica'),
				'weight' => 1,
			)
		);
		vc_add_param('vc_tta_tabs', array(
				'type' => 'dropdown',
				'param_name' => 'style',
				'value' => array(
					esc_html__('topica', 'topica') => 'tab_style1',
					esc_html__('Classic', 'topica') => 'classic',
					esc_html__('Modern', 'topica') => 'modern',
					esc_html__('Flat', 'topica') => 'flat',
					esc_html__('Outline', 'topica') => 'outline',
				),
				'heading' => esc_html__('Style', 'topica'),
				'description' => esc_html__('Select tabs display style.', 'topica'),
				'weight' => 1,
			)
		);
		vc_add_param('vc_tta_tour', array(
				'type' => 'dropdown',
				'param_name' => 'style',
				'value' => array(
					esc_html__('topica', 'topica') => 'tour_style1',
					esc_html__('Classic', 'topica') => 'classic',
					esc_html__('Modern', 'topica') => 'modern',
					esc_html__('Flat', 'topica') => 'flat',
					esc_html__('Outline', 'topica') => 'outline',
				),
				'heading' => esc_html__('Style', 'topica'),
				'description' => esc_html__('Select tabs display style.', 'topica'),
				'weight' => 1,
			)
		);

		$settings_vc_map = array(
			'category' => array(esc_html__('Content', 'topica'), esc_html__('topica Shortcodes', 'topica'))
		);
		vc_map_update('vc_tta_tabs', $settings_vc_map);
		vc_map_update('vc_tta_tour', $settings_vc_map);
		vc_map_update('vc_tta_accordion', $settings_vc_map);
	}
}

topica_add_vc_param();