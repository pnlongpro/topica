<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 12/29/2016
 * Time: 10:37 AM
 */
/*================================================
LOAD STYLESHEETS
================================================== */
if (!function_exists('topica_enqueue_styles')) {
	function topica_enqueue_styles() {

		/*font-awesome*/
//		$url_font_awesome = THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome.min.css';
//		if (isset($topica_options['cdn_font_awesome']) && !empty($topica_options['cdn_font_awesome'])) {
//			$url_font_awesome = $topica_options['cdn_font_awesome'];
//		}
//		wp_enqueue_style('topica_framework_font_awesome', $url_font_awesome, array());
//		wp_enqueue_style('topica_framework_font_awesome_animation', THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome-animation.min.css', array());
//
//		/*bootstrap*/
//		$url_bootstrap = THEME_URL . 'assets/plugins/bootstrap/css/bootstrap.min.css';
//		if (isset($topica_options['cdn_bootstrap_css']) && !empty($topica_options['cdn_bootstrap_css'])) {
//			$url_bootstrap = $topica_options['cdn_bootstrap_css'];
//		}
		$url_bootstrap = THEME_URL . 'assets/css/bootstrap.min.css';
		wp_enqueue_style('topica_framework_bootstrap', $url_bootstrap, array());

		/*main style*/
		wp_enqueue_style('topica_main_style', THEME_URL . 'assets/css/style.css', array());

		wp_enqueue_style('topica_framework_style', THEME_URL . 'style.css');
		if( is_singular('post') || is_category() || is_page_template('page-template/template-tin-tuc.php') || is_search() ){
			wp_enqueue_style('topica_framework_blog', THEME_URL . 'assets/css/blog.css');
		}
	}
	add_action('wp_enqueue_scripts', 'topica_enqueue_styles',11);
}
if (!function_exists('topica_enqueue_script')) {
	function topica_enqueue_script() {
//		wp_enqueue_script('jquery');
		/*plugins*/
		wp_enqueue_script('topica_framework_jquery_sticky', THEME_URL . 'assets/js/lib/jquery.sticky.js', array(), false, true);
		wp_enqueue_script('topica_framework_plugins', THEME_URL . 'assets/js/lib/plugins.js', array(), false, true);
		/*bootstrap*/
		$url_bootstrap = THEME_URL . 'assets/js/lib/bootstrap-v3.min.js';
		wp_enqueue_script('topica_framework_bootstrap', $url_bootstrap, array('jquery'), false, true);

		if (is_single()) {
			wp_enqueue_script('comment-reply');
		}

		/*modernizr*/
		wp_enqueue_script('topica_framework_modernizr', THEME_URL . 'assets/js/lib/modernizr-prod.min.js', array(),false, false);



		/*global*/
		wp_enqueue_script('topica_framework_global', THEME_URL . 'assets/js/lib/global.js', array(), false, true);

		wp_enqueue_script('topica_framework_main', THEME_URL . 'assets/js/lib/main.js', array(), false, true);


//		wp_enqueue_script('topica_framework_analytics', THEME_URL . 'assets/js/lib/analytics-scroll.js', array(), false, true);

//		wp_localize_script('topica_framework_app', 'topica_framework_ajax_url', get_site_url() . '/wp-admin/admin-ajax.php?activate-multi=true');
//		wp_localize_script('topica_framework_app', 'topica_framework_theme_url', THEME_URL);
//		wp_localize_script('topica_framework_app', 'topica_framework_site_url', site_url());


	}
	add_action('wp_enqueue_scripts', 'topica_enqueue_script');
}
function modify_jquery_version() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery',
			'https://code.jquery.com/jquery-1.12.4.min.js', false, '2.0.s');
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'modify_jquery_version');