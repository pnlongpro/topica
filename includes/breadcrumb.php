<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/3/2015
 * Time: 10:20 AM
 */
?>
<?php if (!is_front_page()) : ?>
	<?php topica_get_breadcrumb(); ?>
<?php else: ?>
	<ul class="breadcrumbs">
		<li><a href="<?php echo home_url('/') ?>" class="home"><?php esc_html_e('Home','topica');?> </a></li>
		<li><span><?php esc_html_e('Blog', 'topica'); ?></span></li>
	</ul>
<?php endif; ?>
