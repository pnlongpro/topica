<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/2/2017
 * Time: 2:02 AM
 */
if ( !class_exists( 'Topica_ChuongTrinhDaoTao' ) ) {
	class Topica_ChuongTrinhDaoTao {
		function __construct() {
			add_action( 'init', array( $this, 'register_post_types' ), 5 );
//			add_action('init', array($this, 'register_taxonomies'), 5);
		}

		function register_post_types() {
			$post_type = 'chuyen-nganh';

			if ( post_type_exists( $post_type ) ) {
				return;
			}

			$post_type_slug = get_option( 'topica-' . $post_type . '-config' );
			if ( !isset( $post_type_slug ) || !is_array( $post_type_slug ) ) {
				$slug = $post_type;
				$name = $singular_name = 'Chuyên ngành đào tạo';
			} else {
				$slug          = $post_type_slug['slug'];
				$name          = $post_type_slug['name'];
				$singular_name = $post_type_slug['singular_name'];
			}

			register_post_type( $post_type,
				array(
					'label'       => esc_html__( 'Chuyên ngành đào tạo', 'topica' ),
					'description' => esc_html__( 'Mô tả', 'topica' ),
					'labels'      => array(
						'name'               => $name,
						'singular_name'      => $singular_name,
						'menu_name'          => $name,
						'parent_item_colon'  => esc_html__( 'Parent Item:', 'topica' ),
						'all_items'          => sprintf( esc_html__( 'Tất cả %s', 'topica' ), $name ),
						'view_item'          => esc_html__( 'View Item', 'topica' ),
						'add_new_item'       => sprintf( esc_html__( 'Add New %s', 'topica' ), $name ),
						'add_new'            => esc_html__( 'Thêm mới', 'topica' ),
						'edit_item'          => esc_html__( 'Chỉnh sửa', 'topica' ),
						'update_item'        => esc_html__( 'Update Item', 'topica' ),
						'search_items'       => esc_html__( 'Search Item', 'topica' ),
						'not_found'          => esc_html__( 'Not found', 'topica' ),
						'not_found_in_trash' => esc_html__( 'Not found in Trash', 'topica' ),
					),
					'supports'    => array( 'title','editor','thumbnail', 'revisions' ),
					'public'      => true,
					'show_ui'     => true,
					'_builtin'    => false,
					'has_archive' => true,
					'rewrite'     => array( 'slug' => $slug, 'with_front' => true ),
				)
			);
			flush_rewrite_rules();
		}

		function register_taxonomies() {
			if ( taxonomy_exists( 'chuyen-nganh' ) ) {
				return;
			}

			$post_type     = 'chuyen-nganh';
			$taxonomy_slug = 'chuyen-nganh';
			$taxonomy_name = 'Chuyên ngành';

			$post_type_slug = get_option( 'topica-' . $post_type . '-config' );
			if ( isset( $post_type_slug ) && is_array( $post_type_slug ) &&
				array_key_exists( 'taxonomy_slug', $post_type_slug ) && $post_type_slug['taxonomy_slug'] != ''
			) {
				$taxonomy_slug = $post_type_slug['taxonomy_slug'];
				$taxonomy_name = $post_type_slug['taxonomy_name'];
			}
			register_taxonomy( 'chuyen-nganh', $post_type,
				array( 'hierarchical' => true,
					   'label'        => $taxonomy_name,
					   'query_var'    => true,
					   'rewrite'      => array( 'slug' => $taxonomy_slug ) )
			);
			flush_rewrite_rules();
		}
	}

	new Topica_ChuongTrinhDaoTao();
}