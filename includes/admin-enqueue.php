<?php
if (!function_exists('topica_admin_enqueue_scripts')) {

	function topica_admin_enqueue_scripts() {
		// Enqueue Script
		wp_enqueue_script( 'topica-admin-app-js', THEME_URL . 'admin/js/admin.app.js',array(), '1.0.0', true );
		$topica_options = &Topica_Global::get_options();
		$meta_boxes = &Topica_Global::get_meta_boxes();
		$meta_box_id = '';
		foreach ($meta_boxes as $box) {
			if (!isset($box['tab'])) {
				continue;
			}
			if (!empty($meta_box_id)) {
				$meta_box_id .= ',';
			}
			$meta_box_id .= '#' . $box['id'];
		}

		wp_localize_script( 'topica-admin-app-js' , 'meta_box_ids' , $meta_box_id);

		// Enqueue CSS
		wp_enqueue_style( 'topica-admin-meta-box-css', THEME_URL . 'admin/css/meta-box.css', false, '1.0.0' );
//		wp_enqueue_style( 'topica-admin-redux-css', THEME_URL . '/admin/assets/css/redux-admin.css', false, '1.0.0' );


		/*topica FontIcon*/

	}
	add_action( 'admin_enqueue_scripts', 'topica_admin_enqueue_scripts' );
}