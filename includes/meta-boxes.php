<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/2/2017
 * Time: 7:13 PM
 */
function topica_register_meta_boxes() {
	$meta_boxes = &Topica_Global::get_meta_boxes();
	$prefix     = 'topica_';

// POST FORMAT: Image
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Image', 'topica'),
		'id' => $prefix .'meta_box_post_format_image',
		'post_types' => array('post'),
		/*'context' => 'side',
		'priority' => 'low',*/
		'fields' => array(
			array(
				'name' => esc_html__('Image', 'topica'),
				'id' => $prefix . 'post_format_image',
				'type' => 'image_advanced',
				'max_file_uploads' => 1,
				'desc' => esc_html__('Select a image for post','topica')
			),
		),
	);

// POST FORMAT: Gallery
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Gallery', 'topica'),
		'id' => $prefix . 'meta_box_post_format_gallery',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__('Images', 'topica'),
				'id' => $prefix . 'post_format_gallery',
				'type' => 'image_advanced',
				'desc' => esc_html__('Select images gallery for post','topica')
			),
		),
	);

// POST FORMAT: Video
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Video', 'topica'),
		'id' => $prefix . 'meta_box_post_format_video',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__( 'Video URL or Embeded Code', 'topica' ),
				'id'   => $prefix . 'post_format_video',
				'type' => 'textarea',
			),
		),
	);

// POST FORMAT: Audio
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Audio', 'topica'),
		'id' => $prefix . 'meta_box_post_format_audio',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__( 'Audio URL or Embeded Code', 'topica' ),
				'id'   => $prefix . 'post_format_audio',
				'type' => 'textarea',
			),
		),
	);

// POST FORMAT: QUOTE
//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Quote', 'topica'),
		'id' => $prefix . 'meta_box_post_format_quote',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__( 'Quote', 'topica' ),
				'id'   => $prefix . 'post_format_quote',
				'type' => 'textarea',
			),
			array(
				'name' => esc_html__( 'Author', 'topica' ),
				'id'   => $prefix . 'post_format_quote_author',
				'type' => 'text',
			),
			array(
				'name' => esc_html__( 'Author Url', 'topica' ),
				'id'   => $prefix . 'post_format_quote_author_url',
				'type' => 'url',
			),
		),
	);
	// POST FORMAT: LINK
	//--------------------------------------------------
	$meta_boxes[] = array(
		'title' => esc_html__('Post Format: Link', 'topica'),
		'id' => $prefix . 'meta_box_post_format_link',
		'post_types' => array('post'),
		'fields' => array(
			array(
				'name' => esc_html__( 'Url', 'topica' ),
				'id'   => $prefix . 'post_format_link_url',
				'type' => 'url',
			),
			array(
				'name' => esc_html__( 'Text', 'topica' ),
				'id'   => $prefix . 'post_format_link_text',
				'type' => 'text',
			),
		),
	);

	// PAGE HEADER
	//--------------------------------------------------
	$meta_boxes[] = array(
		'id'         => $prefix . 'page_header_meta_box',
		'title'      => esc_html__( 'Page Header', 'topica' ),
		'post_types' => array( 'post', 'page','chuyen-nganh' ),
		'tab'        => true,
		'fields'     => array(
			array(
				'name' => esc_html__( 'Banner Setting', 'topica' ),
				'id'   => $prefix . 'page_header_banner',
				'type' => 'section',
				'std'  => '',
			),
			array(
				'name'    => esc_html__( 'Show/Hide Banner', 'topica' ),
				'id'      => $prefix . 'show_hide_banner',
				'type'    => 'button_set',
				'std'     => '0',
				'options' => array(
					'1'  => esc_html__( 'Show Banner Section', 'topica' ),
					'0'  => esc_html__( 'Hide Banner Section', 'topica' )
				),
				'desc'    => esc_html__( 'Show/hide Banner Section', 'topica' ),
			),
			array(
				'name'    => esc_html__( 'Banner Layout', 'topica' ),
				'id'      => $prefix . 'header_banner_layout',
				'type'    => 'button_set',
				'std'     => '1',
				'options' => array(
					'1'  => esc_html__( 'Image', 'topica' ),
					'0'  => esc_html__( 'Video', 'topica' )
				),
				'required-field' => array( $prefix . 'show_hide_banner', '=', array( '1' ) ),
			),
			array(
				'name'    => esc_html__( 'Select Image ', 'topica' ),
				'id'      => $prefix . 'header_banner_image',
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'required-field' => array( $prefix . 'header_banner_layout', '=', array( '1' ) ),
			),
			array(
				'name'    => esc_html__( 'Video Iframe', 'topica' ),
				'id'      => $prefix . 'header_banner_video',
				'type'             => 'textarea',
				'required-field' => array( $prefix . 'header_banner_layout', '=', array( '0' ) ),
			),
			array(
				'name'    => esc_html__( 'Banner Title', 'topica' ),
				'id'      => $prefix . 'header_banner_title',
				'type'             => 'text',
				'required-field' => array( $prefix . 'show_hide_banner', '=', array( '1' ) ),
			),
			array(
				'name'    => esc_html__( 'Banner Description', 'topica' ),
				'id'      => $prefix . 'header_banner_description',
				'type'             => 'textarea',
				'required-field' => array( $prefix . 'show_hide_banner', '=', array( '1' ) ),
			),
		)
	);
	//Page Layout
	$meta_boxes[] = array(
		'id' => $prefix . 'page_layout_meta_box',
		'title' => esc_html__('Page Layout', 'topica'),
		'post_types' => array('post', 'page',  'chuyen-nganh'),
		'tab' => true,
		'fields' => array(
			array(
				'name'  => esc_html__( 'Layout Style', 'topica' ),
				'id'    => $prefix . 'layout_style',
				'type'  => 'button_set',
				'options' => array(
					'-1' => esc_html__('Default','topica'),
					'layout-1'	  => esc_html__('Layout 1','topica'),
					'layout-2'	  => esc_html__('Layout 2','topica')
				),
				'std'	=> '-1',
				'multiple' => false,
			),
			array(
				'name'  => esc_html__( 'Page Sidebar', 'topica' ),
				'id'    => $prefix . 'page_sidebar',
				'type'  => 'image_set',
				'allowClear' => true,
				'options' => array(
					'none'	  => THEME_URL.'/assets/img/theme-options/sidebar-none.png',
					'left'	  => THEME_URL.'/assets/img/theme-options/sidebar-left.png',
					'right'	  => THEME_URL.'/assets/img/theme-options/sidebar-right.png',
				),
				'std'	=> 'none',
				'multiple' => false,

			),
			array (
				'name' 	=> esc_html__('Left Sidebar', 'topica'),
				'id' 	=> $prefix . 'page_left_sidebar',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','topica'),
				'std' 	=> '',
				'required-field' => array($prefix . 'page_sidebar','=',array('','left','both')),
			),

			array (
				'name' 	=> esc_html__('Right Sidebar', 'topica'),
				'id' 	=> $prefix . 'page_right_sidebar',
				'type' 	=> 'sidebars',
				'placeholder' => esc_html__('Select Sidebar','topica'),
				'std' 	=> '',
				'required-field' => array($prefix . 'page_sidebar','=',array('','right','both')),
			),
			array(
				'name'  => esc_html__( 'Sidebar Type', 'topica' ),
				'id'    => $prefix . 'sidebar_type',
				'type'  => 'button_set',
				'options' => array(
					'-1' => esc_html__('Default','topica'),
					'style-1'	  => esc_html__('style 1','topica'),
					'style-2'	  => esc_html__('style 2','topica')
				),
				'std'	=> '-1',
				'multiple' => false,
				'required-field' => array($prefix . 'page_sidebar','<>',array('none')),
			),
			array (
				'name' 	=> esc_html__('Page Class Extra', 'topica'),
				'id' 	=> $prefix . 'page_class_extra',
				'type' 	=> 'text',
				'std' 	=> ''
			),
		)
	);
	// PAGE FOOTER
	//--------------------------------------------------
	$meta_boxes[] = array(
		'id'         => $prefix . 'page_footer_meta_box',
		'title'      => esc_html__( 'Page Footer', 'topica' ),
		'post_types' => array( 'post', 'page', 'chuyen-nganh' ),
		'tab'        => true,
		'fields'     => array(
			array(
				'name' => esc_html__( 'Footer Settings', 'topica' ),
				'id'   => $prefix . 'page_footer_section_1',
				'type' => 'section',
				'std'  => '',
			),
			array(
				'name'    => esc_html__( 'Show/Hide Button Section', 'topica' ),
				'id'      => $prefix . 'footer_show_hide_button',
				'type'    => 'button_set',
				'std'     => '-1',
				'options' => array(
					'-1' => esc_html__( 'Default', 'topica' ),
					'show'  => esc_html__( 'Show Search Section', 'topica' ),
					'hide'  => esc_html__( 'Hide Search Section', 'topica' )
				),
				'desc'    => esc_html__( 'Show/hide Search Section', 'topica' ),
			),
			array(
				'name'    => esc_html__( 'Show/Hide Search Section', 'topica' ),
				'id'      => $prefix . 'footer_show_hide_search',
				'type'    => 'button_set',
				'std'     => '-1',
				'options' => array(
					'-1' => esc_html__( 'Default', 'topica' ),
					'show'  => esc_html__( 'Show Search Section', 'topica' ),
					'hide'  => esc_html__( 'Hide Search Section', 'topica' )
				),
				'desc'    => esc_html__( 'Show/hide Search Section', 'topica' ),
			),
		)
	);

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( class_exists( 'RW_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			new RW_Meta_Box( $meta_box );
		}
	}
}
add_action( 'admin_init', 'topica_register_meta_boxes' );