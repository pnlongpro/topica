<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 12/31/2016
 * Time: 10:09 AM
 */

//Template Name: Academics
?>

<?php get_header() ?>
<div class="nav-sticky-wrapper">
	<!-- LOCAL SECTION NAV -->
	<div class="container local-nav-wrapper">
		<nav class="row">
			<ul class="col-sm-12 local-nav">
				<li><a href="#degrees-programs">TỔNG QUAN CHƯƠNG TRÌNH</a>
				</li>
				<li><a href="#departments">CHƯƠNG TRÌNH ĐÀO TẠO</a>
				</li>
				<li><a href="#online-abroad">PHƯƠNG PHÁP HỌC TẬP</a>
				</li>
				<li><a href="#honors">CHƯƠNG TRÌNH HỌC BỔNG</a>
				</li>
				<li><a href="#academic-support">DỊCH VỤ TIỆN ÍCH</a>
				</li>
			</ul>
		</nav>
	</div>
</div>

<div id="main-content">
	<!-- DEGREES & PROGRAMS -->
	<section id="degrees-programs" class="degrees-programs row" aria-label="Degrees &amp; Programs">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<h2><?php echo get_field( 'academics_heading_1' ) ?></h2>

					<?php echo get_field( 'academics_description_1' ) ?>
					<form class="form large-search" method="get" action="<?php echo home_url(); ?>" role="search" >
						<div class="search-wrapper"><span class="overlay-icon icon-search"></span>
							<button class="largeSearchBtn search-btn" type="submit">
								<span class="icon-chevron-right"><span class="sr-only">Search</span></span>
							</button>
							<input name="s" type="text" placeholder="Bạn đang tìm kiếm?..." class="big-search search-large" title="Type here to start searching...">
						</div>
					</form>
					<div class="search-link">hoặc
						<a href="<?php echo home_url() ?>/site-map">Xem cấu trúc Website tại đây</a>

					</div>
				</div>
				<div class="col-sm-6">
					<a href="<?php echo get_field( 'academics_video_url_1' ) ?>" class="wistia-popover[height=388,playerColor=7b796a,width=640]” video-wrapper">
						<?php $image1 = get_field( 'academics_video_thumbnail_1' ); ?>
						<img src="<?php echo $image1['url'] ?>" alt="<?php echo $image1['title'] ?>">
						<div class="video-caption">
							<?php echo get_field( 'academics_video_video_caption_1' ) ?>
						</div>
						<span class="btn-outer"><span class="btn-inner">&nbsp;</span></span>
					</a>
				</div>
			</div>
		</div>
	</section>


	<!-- COLLEGES & SCHOOLS -->
	<section id="departments" class="departments" aria-label="Colleges &amp; Schools">
		<div class="container">
			<header class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<h1><?php echo get_field( 'academics_heading_2' ) ?></h1>

					<?php echo get_field( 'academics_description_2' ) ?>
				</div>
			</header>
			<?php
			$infoboxs1 = get_field( 'academics_info_boxs_1' );
			if ( $infoboxs1 ) :
				?>
				<div class="content-wrap">
					<?php
					foreach ( $infoboxs1 as $key => $value ) :
						$class_thumb = $key % 2 == 0 ? 'col-sm-7' : 'col-sm-7 col-sm-push-5';
						$class_description = $key % 2 == 0 ? 'col-sm-5' : 'col-sm-5 col-sm-pull-7';
						$title = $value['title'] != '' ? $value['title'] : '';
						$heading_color = $value['heading_color'] != '' ? $value['heading_color'] : '#5292bc';
						$description = $value['description'] != '' ? $value['description'] : '';
						$link = $value['link'] != '' ? $value['link'] : '#';
						$image = $value['image_thumbnail'] != '' ? $value['image_thumbnail'] : '';
						$style = 'style="color: ' . $heading_color . '"'
						?>
						<div class="content-row col-sm-12">
							<div class="row">
								<div class="<?php echo $class_thumb ?>">
									<?php if ( $image ) : ?>
										<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
									<?php endif; ?>
								</div>
								<div class="section-text <?php echo $class_description ?>">
									<h4 <?php echo $style ?>><?php echo esc_html( $title ) ?></h4>

									<div style="color:#3d3b3a"><?php echo $description ?></div>
									<a href="<?php echo $link; ?>" class="btn btn-solid btn-light-blue">TÌM HIỂU THÊM</a>

								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>

			<?php endif; ?>
		</div>
	</section>
</div>
<section id="online-abroad" class="online-abroad row" aria-label="Online Learning / Studying Abroad">
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1">
			<header class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<h2><?php echo get_field( 'academics_heading_3' ) ?></h2>

					<?php echo get_field( 'academics_description_3' ) ?>
				</div>
			</header>
			<?php
			$infoboxs2 = get_field( 'academics_info_boxs_3' );
			if ( $infoboxs2 ) :
				?>
				<div class="row content-wrap">
					<div class="content-row blue col-sm-12">
						<?php
						foreach ( $infoboxs2 as $key => $value ) :
							$class_thumb = $key % 2 == 0 ? 'col-sm-7' : 'col-sm-7 col-sm-push-5';
							$class_description = $key % 2 == 0 ? 'col-sm-5' : 'col-sm-5 col-sm-pull-7';
							$title = $value['title'] != '' ? $value['title'] : '';
							$description = $value['description'] != '' ? $value['description'] : '';
							$link = $value['link'] != '' ? $value['link'] : '#';
							$image = $value['image_thumbnail'] != '' ? $value['image_thumbnail'] : '';
							?>
							<div class="row">
								<div class="<?php echo $class_thumb ?>">
									<?php if ( $image ) : ?>
										<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
									<?php endif; ?>
								</div>
								<div class="section-text <?php echo $class_description ?>">
									<h4><?php echo esc_html( $title ) ?></h4>

									<div style="color:#3d3b3a"><?php echo $description ?></div>
									<a href="<?php echo $link; ?>" class="btn btn-solid btn-light-blue">TÌM HIỂU THÊM</a>

								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>

		</div>
	</div>
</section>

<section id="honors" class="honors row" aria-label="Honors Section">
	<div class="container">
		<div class="col-sm-8 col-sm-offset-2">
			<h2><?php echo get_field( 'academics_heading_4' ) ?></h2>

			<?php echo get_field( 'academics_description_4' ) ?>
			<a href="<?php echo get_field( 'academics_link_4' ) != '' ? get_field( 'academics_link_4' ) : '#' ?>" class="btn">TÌM HIỂU NGAY</a>
		</div>
	</div>
</section>
<section id="academic-support" class="academic-support row" aria-label="Academic Support Section">
	<div class="container">
		<div class="row">
			<header class="col-sm-8 col-sm-offset-2">
				<h2><?php echo get_field( 'academics_heading_5' ) ?></h2>

				<?php echo get_field( 'academics_description_5' ) ?>
			</header>
		</div>
		<?php
			$infoboxs3 = get_field('academics_info_boxs_5');
			if( $infoboxs3 ) :
		?>
		<div class="support-items row">
			<?php
				foreach( $infoboxs3 as $value ) :
					$title = $value['title'] != '' ? $value['title'] : '';
					$description = $value['description'] != '' ? $value['description'] : '';
					$link = $value['link'] != '' ? $value['link'] : '#';
			?>
				<div class="col-sm-4">
					<div class="item-wrapper">
						<h3><?php echo $title ?></h3>

						<?php echo $description ?>
						<a href="<?php echo $link ?>" class="btn btn-blue">TÌM HIỂU THÊM</a>

					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
</section>

<?php get_footer() ?>
