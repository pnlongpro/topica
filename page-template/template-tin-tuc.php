<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 1/10/2017
 * Time: 10:03 PM
 */
//Template Name: News

get_header();
?>
<div class="vlog-section ">
	<div class="container">
		<div class="vlog-content">
			<div class="row">
				<div class="vlog-module module-posts col-lg-12 col-md-12 col-sm-12" id="vlog-module-2-0" data-col="12">
					<section id="news" class="blog row" aria-label="news">
						<div class="vlog-mod-head">
							<div class="vlog-mod-title">
								<h4 style="font-family:verdana;">Tin tức ivyuni</h4></div>
						</div>
						<?php
							$list_news = get_field('select_category_news');
							$args = array(
								'post_type'            => 'post',
								'ignore_sticky_posts'  => 1,
								'no_found_rows'        => 1,
								'posts_per_page'       => 3,
								'tax_query'            => array(
									array(
										'taxonomy' => 'category',
										'field' => 'id',
										'terms' => $list_news,
										'operator' => 'IN'
									)
								)
							);
						$loop_new = new WP_Query($args);
						if( $loop_new->have_posts() ) :
						?>
						<div class="row vlog-posts row-eq-height ">
							<?php while( $loop_new->have_posts() ) : $loop_new ->the_post(); ?>
								<?php get_template_part('template-parts/content') ?>
							<?php endwhile; ?>
						<?php
								wp_reset_postdata();
								wp_reset_query();
							?>
						</div>
						<?php endif; ?>
						<nav class="vlog-pagination">
							<a class="next page-numbers" href="<?php echo get_field('link_news') ?>">Xem tất cả Tin tức</a>
						</nav>
					</section>
					<section id="blog" class="blog row" aria-label="blog">
						<div class="vlog-mod-head">
							<div class="vlog-mod-title">
								<h4 style="font-family:verdana;">BLOG ivyuni</h4></div>
						</div>

						<?php
						$list_blog = get_field('select_category_blog');
						$args = array(
							'post_type'            => 'post',
							'ignore_sticky_posts'  => 1,
							'no_found_rows'        => 1,
							'posts_per_page'       => 3,
							'tax_query'            => array(
								array(
									'taxonomy' => 'category',
									'field' => 'id',
									'terms' => $list_blog,
									'operator' => 'IN'
								)
							)
						);
						$loop_blog = new WP_Query($args);
						if( $loop_blog->have_posts() ) :
							?>
							<div class="row vlog-posts row-eq-height ">
								<?php while( $loop_blog->have_posts() ) : $loop_blog ->the_post(); ?>
									<?php get_template_part('template-parts/content') ?>
								<?php endwhile; ?>
								<?php
								wp_reset_postdata();
								wp_reset_query();
								?>
							</div>
						<?php endif; ?>

						<nav class="vlog-pagination">

							<a class="next page-numbers" href="<?php echo get_field('link_blog') ?>">Xem tất cả Blog</a> </nav>
					</section>

				</div>

			</div>

		</div>

		<div class="vlog-sidebar vlog-sidebar-right">
			<?php dynamic_sidebar('widget-area-1') ?>
		</div>

	</div>

</div>
<?php get_footer(); ?>
