<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 12/31/2016
 * Time: 10:09 AM
 */

//Template Name: Career
?>

<?php get_header() ?>
<div class="nav-sticky-wrapper">
	<!-- LOCAL SECTION NAV -->
	<div class="container local-nav-wrapper">
		<nav class="row">
			<ul class="col-sm-12 local-nav">
				<li><a href="#economic-development">Chương trình thực tập</a>
				</li>
				<li><a href="#community-outreach">Những con số ấn tượng</a>
				</li>
			</ul>
		</nav>
	</div>
</div>

<div id="main-content" class="page-template-template-scholarship">
	<!-- CHƯƠNG TRÌNH THỰC TẬP -->
	<section id="economic-development" class="economic-development" aria-label="Economic Development">
		<div class="container">
			<div class="row intro">
				<header class="col-sm-10 col-sm-offset-1">
					<h2><?php echo get_field( 'career_heading_1' ) ?></h2>
					<?php echo get_field( 'career_description_1' ) ?>
					<article style="margin-top: -70px;" class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
						<a href="<?php echo get_field( 'career_video_url_1' ) ?>" class="wistia-popover[height=360,playerColor=7b796a,width=640] video-wrapper">
							<?php $img = get_field( 'career_video_thumbnail_1' ) ?>
							<img alt="“Impact Video" src="<?php echo $img['url'] ?>">
							<div class="video-caption">
								<?php echo get_field( 'career_video_video_caption_1' ) ?>
							</div>
							<span class="btn-outer"><span class="btn-inner">&nbsp;</span></span>
						</a>
					</article>
				</header>
			</div>
			<?php
			$info_boxs1 = get_field( 'career_info_boxs_1' );
			if ( $info_boxs1 ) :
				?>
				<div class="col-sm-10 col-sm-offset-1">
					<?php
					foreach ( $info_boxs1 as $key => $value ) :
						$class_image = $key % 2 == 0 ? 'col-sm-5' : 'col-sm-5 col-sm-push-7';
						$class_content = $key % 2 == 0 ? 'col-sm-7' : 'col-sm-7 col-sm-pull-5';
						$title = $value['title'] != '' ? $value['title'] : '';
						$description = $value['description'] != '' ? $value['description'] : '';
						$image = $value['image'] != '' ? $value['image'] : '';
						$link = $value['link'] != '' ? $value['link'] : '#';
						?>
						<article class="row content-block">
							<div class="<?php echo $class_image ?>">
								<?php if ( $image ) : ?>
									<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
								<?php endif; ?>
							</div>
							<div class="<?php echo $class_content ?>">
								<h3><?php echo $title; ?></h3>
								<?php echo $description; ?>
							</div>
						</article>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
	<!-- COMMUNITY OUTREACH -->
	<section id="community-outreach" class="community-outreach" aria-label="Community Outreach">
		<div class="container">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="row intro">
					<header class="col-sm-12">
						<h2><?php echo get_field( 'career_heading_2' ) ?></h2>
						<?php echo get_field( 'career_description_2' ) ?>
					</header>
				</div>
				<div class="row content-wrap">
					<?php
					$info_boxs2 = get_field( 'career_contact_box' );
					if ( $info_boxs2 ) :
						?>
						<div class="content-row blue col-sm-12">
							<div class="row">
								<?php foreach ( $info_boxs2 as $value ) : ?>
									<div class="col-sm-6 col-sm-push-6 content-links">
										<img src="<?php echo $value['image']['url'] ?>" alt="<?php echo $value['image']['title'] ?>">
									</div>
									<div class="section-text coop-ext col-sm-6 col-sm-pull-6">
										<h3><?php echo $value['title'] ?></h3>

										<?php echo $value['description'] ?>
										<div class="center-btn">
											<a class="btn btn-blue" href="<?php echo $value['link'] != '' ? $value['link'] : '#'; ?>">Liên hệ</a>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>
					<div class="col-sm-12">
						<div class="arts-grid">
							<header class="arts-figures">
								<h3><?php echo get_field( 'caree_bottom_heading' ) ?></h3>
							</header>
							<?php
							$info_boxs3 = get_field( 'career_info_boxs_2' );
							if ( $info_boxs3 ) :
							echo '<div class="grid-row row">';
								foreach ( $info_boxs3 as $key => $value ) :
										if( $key % 2 == 0 && $key != 0){
											echo '</div><div class="grid-row row">';
										}
									?>
										<div class="grid-item col-sm-6">
											<div class="grid-figure countFade countUp"><?php echo $value['value'] ?></div>
											<?php echo $value['description'] ?>
										</div>

								<?php endforeach; ?>
						</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>


<?php get_footer() ?>
