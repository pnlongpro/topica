<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 12/31/2016
 * Time: 10:09 AM
 */

//Template Name: Student
?>

<?php get_header() ?>
<div class="nav-sticky-wrapper">
	<!-- LOCAL SECTION NAV -->
	<div class="container local-nav-wrapper">
		<nav class="row">
			<ul class="col-sm-12 local-nav">
				<li><a href="#quality-of-life">QUYỀN LỢI SINH VIÊN</a>
				</li>
				<li><a href="#life-together">HOẠT ĐỘNG SINH VIÊN</a>
				</li>
				<li><a href="#legacy">DỊCH VỤ HỖ TRỢ</a>
				</li>
			</ul>
		</nav>
	</div>
</div>

<div id="main-content">


	<!-- QUALITY OF LIFE -->
	<section class="quality-life" aria-label="Quality of Life section">
		<div class="container">
			<div class="row intro">
				<header class="col-sm-7">
					<h2><?php echo get_field( 'student_heading_1' ) ?></h2>

					<?php echo get_field( 'student_description_1' ) ?>
				</header>
				<article class="col-sm-5">
					<a href="<?php echo get_field( 'student_video_url_1' ) ?>" class="wistia-popover[height=388,playerColor=7b796a,width=640]” video-wrapper">
						<?php $image1 = get_field( 'student_video_thumbnail_1' ); ?>
						<img src="<?php echo $image1['url'] ?>" alt="<?php echo $image1['title'] ?>">
						<div class="video-caption">
							<?php echo get_field( 'student_video_video_caption_1' ) ?>
						</div>
						<span class="btn-outer"><span class="btn-inner">&nbsp;</span></span>
					</a>
				</article>
			</div>
			<div id="quality-of-life" class="photo-box row">
				<header class="col-sm-8 col-sm-offset-2">
					<h2><?php echo get_field( 'student_heading_2' ) ?></h2>
				</header>
				<?php
				$list_info_2 = get_field( 'student_infoboxs' );
				if ( $list_info_2 ) :
					?>
					<div class="col-sm-10 col-sm-offset-1">
						<!-- PHOTOBOX -->
						<ul id="photoBox" class="campus-photo-box">
							<?php
							foreach ( $list_info_2 as $item ) :
								$link = $item['link'] != '' ? $item['link'] : '#';
								$title = $item['title'] != '' ? $item['title'] : '';
								$sub_title = $item['Description'] != '' ? $item['Description'] : '';
								$thumbnail = $item['thumbnail'] != '' ? $item['thumbnail'] : '';
								$bg_color = $item['background_color'] != '' ? $item['background_color'] : '';
								$style = '';
								if ( $bg_color != '' ) {
									$style = 'style="background-color: ' . $bg_color . '"';
								}
								?>
								<li>
									<a href="<?php echo $link; ?>">
										<img src="<?php echo $thumbnail['url'] ?>" alt="<?php echo $thumbnail['title'] ?>" />
									<span class="photo-label-wrap">
										<span class="photo-label top-left"><?php echo $title ?></span>
									</span>
										<span class="overlay-photo"></span>
									<span class="overlay-text" <?php echo $style ?>>
										<span class="ov-header"><?php echo $title ?></span>
									 	<span class="ov-text"><?php echo $sub_title ?></span>
									  	<span class="ov-btn btn">TÌM HIỂU THÊM</span>
									</span>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<!-- LIFE TOGETHER-->
	<section id="life-together" class="life-together" aria-label="Life Together section">
		<div class="container">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="row intro">
					<header class="col-sm-8 col-sm-offset-2">
						<h2><?php echo get_field('student_heading_3') ?></h2>
						<?php echo get_field('student_description_3') ?>
					</header>
				</div>
				<?php
					$list_info_3 = get_field('academics_info_boxs_3');
					if( $list_info_3 ) :
				?>
				<div class="row content-wrap">
					<div class="content-row blue col-sm-12">
						<?php
						foreach ( $list_info_3 as $key => $value ) :
							$class_image = $key % 2 == 0 ? 'col-sm-7' : 'col-sm-7 col-sm-push-5';
							$class_content = $key % 2 == 0 ? 'col-sm-5' : 'col-sm-5 col-sm-pull-7';
							$title = $value['title'] != '' ? $value['title'] : '';
							$description = $value['description'] != '' ? $value['description'] : '';
							$image = $value['image_thumbnail'] != '' ? $value['image_thumbnail'] : '';
							$link = $value['link'] != '' ? $value['link'] : '#';
							?>
						<div class="row">
							<div class="<?php echo $class_image ?>">
								<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
							</div>
							<div class="section-text <?php echo $class_content ?>">
								<h3><?php echo $title ?></h3>

								<?php echo $description; ?>
								<div><a class="btn btn-solid btn-light-blue" href="<?php echo $link ?>">Tìm hiểu thêm</a>

								</div>
							</div>
						</div>
							<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<!-- OUR LEGACY -->
	<section id="legacy" class="our-legacy row" aria-label="Our Legacy section">
		<div class="container">
			<div class="col-sm-10 col-sm-offset-1">
				<header class="row">
					<h2><?php echo get_field('student_heading_4'); ?></h2>
					<?php echo get_field('student_description_4'); ?>
				</header>
				<?php $list_info_4 = get_field('student_info_boxs_4') ?>
				<?php if( $list_info_4 ) : ?>
				<div class="content-row">
					<?php
					foreach ( $list_info_4 as $key => $value ) :
					$class_image = $key % 2 == 0 ? 'col-sm-6 col-sm-push-6' : 'col-sm-6';
					$class_content = $key % 2 == 0 ? 'col-sm-6 col-sm-pull-6' : 'col-sm-6';
					$title = $value['title'] != '' ? $value['title'] : '';
					$description = $value['description'] != '' ? $value['description'] : '';
					$image = $value['image_thumbnail'] != '' ? $value['image_thumbnail'] : '';
					$link = $value['link'] != '' ? $value['link'] : '#';
					?>
					<div class="border-btm row">
						<div class="<?php echo $class_image ?>">
							<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
						</div>
						<div class="<?php echo $class_content ?>">
							<h3><?php echo $title ?></h3>
							<?php echo $description; ?>
							<div><a  class="btn btn-solid btn-light-blue" href="<?php echo $link ?>">Tìm hiểu thêm</a>

							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>

</div>


<?php get_footer() ?>
