<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 12/31/2016
 * Time: 10:09 AM
 */

//Template Name: Scholarship
?>

<?php get_header() ?>
<div class="nav-sticky-wrapper">
	<!-- LOCAL SECTION NAV -->
	<div class="container local-nav-wrapper">
		<nav class="row">
			<ul class="col-sm-12 local-nav">
				<li><a href="#gioi-thieu-chuong-trinh" class="">GIỚI THIỆU CHƯƠNG TRÌNH</a>
				</li>
				<li><a href="#arts">THÔNG TIN HỌC BỔNG</a>
				</li>
			</ul>
		</nav>
	</div>
</div>

<div id="main-content">
	<!-- Powered By Knowledge -->
	<section id="gioi-thieu-chuong-trinh" class="poweredby" aria-label="The New Nevada Powered is Powered by Knowledge">
		<div class="container">
			<div class="intro row">
				<header class="col-sm-6">
					<h2><?php echo get_field( 'scholarship_heading_1' ) ?></h2>

					<br>
					<a href="<?php echo get_field( 'scholarship_button_link_1' ) != '' ? get_field( 'scholarship_button_link_1' ) : '#' ?>" class="btn cta-btn">TÌM HIỂU NGAY</a>
				</header>
				<article class="col-sm-6">
					<?php echo get_field( 'scholarship_video_script' ) ?>
				</article>

			</div>
		</div>
	</section>

	<!-- ARTS -->
	<section id="arts" class="arts row" aria-label="Arts">
		<div class="container">
			<div class="col-sm-12">
				<header class="intro">
					<h2><?php echo get_field( 'scholarship_heading_2' ) ?></h2>
					<?php echo get_field( 'scholarship_description_2' ) ?>
				</header>
			</div>
			<div class="col-sm-12">
				<header class="arts-figures">
					<h3><?php echo get_field( 'scholarship_heading_3' ) ?></h3>
					<?php echo get_field( 'scholarship_description_3' ) ?>

				</header>
				<?php
				$info_boxs_1 = get_field( 'scholarship_info_boxs_3' );
				if ( $info_boxs_1 ) :
					?>
					<div class="arts-grid">
						<div class="grid-row row">
							<?php
							foreach ( $info_boxs_1 as $key => $value ) :
								if ( $key == '3' ) {
									echo '</div><div class="grid-row row">';
								}
								$value_counter = $value['value_counter'] != '' ? $value['value_counter'] : '0';
								$description   = $value['description'] != '' ? $value['description'] : '';
								$counter_style = $value['counter_style'] == 'countNoComma' ? 'countNoComma' : '';
								?>
								<div class="grid-item col-sm-4">
									<div class="grid-figure countFade countUp <?php echo $counter_style ?>" style="opacity: 1;"><?php echo $value_counter ?></div>
									<?php echo $description ?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div class="community-influence row">
				<header class="col-sm-12">
					<h3><?php echo get_field( 'scholarship_heading_4' ) ?></h3>
					<?php echo get_field( 'scholarship_description_4' ) ?>

				</header>
				<?php
				$info_boxs_2 = get_field( 'scholarship_info_boxs_4' );
				if ( $info_boxs_2 ) :
					?>
					<div class="info-boxes col-sm-12">
						<?php
						foreach ( $info_boxs_2 as $value ) :
							$title = $value['title'] != '' ? $value['title'] : '';
							$description = $value['description'] != '' ? $value['description'] : '';
							$link = $value['link'] != '' ? $value['link'] : '#';
							$image = $value['image_thumbnail'] != '' ? $value['image_thumbnail'] : '';
							?>
							<a href="<?php echo $link ?>" class="box col-sm-4">
								<div class="box-img">
									<?php if ( $image ) : ?>
										<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>" />
									<?php endif; ?>
									<div class="box-overlay"><?php echo $description ?></div>
								</div>
								<div class="box-caption">
									<p><?php echo $title ?></p>
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="student-opportunities col-sm-12">
				<header>
					<h3><?php echo get_field( 'scholarship_heading_5' ) ?></h3>
					<?php echo get_field( 'scholarship_description_5' ) ?>

				</header>
				<?php
				$scholarship_info_boxs_left  = get_field( 'scholarship_info_boxs_left_5' );
				$scholarship_info_boxs_right = get_field( 'scholarship_info_boxs_right_5' );
				$scholarship_info_boxs       = array_merge( $scholarship_info_boxs_left, $scholarship_info_boxs_right );
				if ( $scholarship_info_boxs ) :
					?>
					<div class="row">
						<div class="flexslider mobile-slider sm-show" style="display:none;">
							<ul class="slides">
								<?php foreach ( $scholarship_info_boxs as $value ) : ?>
									<?php
										$image = $value['image_thumbnail'];
										if( is_numeric($image) ){
											$image_attr = wp_get_attachment_image_src( $value['image_thumbnail'],'full' );
											$image_src = $image_attr[0];
										}else{
											$image_src = $image['url'];
										}
									?>
									<li>
										<img src="<?php echo $image_src ?>" alt="">
										<div class="caption"><?php echo $value['title'] ?></div>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				<?php endif; ?>
				<div class="sm-hide col-sm-12">
					<div class="photo-grid">
						<?php
						$scholarship_info_boxs_left  = get_field( 'scholarship_info_boxs_left_5' );
						$scholarship_info_boxs_right = get_field( 'scholarship_info_boxs_right_5' );
						?>
						<div class="row">
							<?php if ( $scholarship_info_boxs_left ) : ?>
								<div class="carousel-col col-sm-6">
									<div class="row">
										<?php
										foreach ( $scholarship_info_boxs_left as $key => $value ) :
											$class_col = $key == 2 ? 'col-sm-12 clearfix' : 'col-sm-6';
											?>
											<div class="<?php echo $class_col ?> grid-margin">
												<div>
													<a href="#">
														<span class="overlay">
															<?php echo $value['description'] ?>
														 </span>
														<span class="title"><?php echo $value['title'] ?></span>
														<img src="<?php echo $value['image_thumbnail']['url'] ?>" alt="<?php echo $value['image_thumbnail']['title'] ?>">
													</a>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							<?php endif; ?>
							<div class="carousel-col col-sm-6">
								<?php if ( $scholarship_info_boxs_right ) : ?>
									<div class="row">
										<?php
										foreach ( $scholarship_info_boxs_right as $key => $item ) :
											$image_arr = wp_get_attachment_image_src( $item['image_thumbnail'],'full' );
//												var_dump($image_arr);
											$class_col = $key == 0 ? 'col-sm-12' : 'col-sm-6';
											?>
											<div class="<?php echo $class_col ?> grid-margin">
												<div>
													<a href="#">
														<span class="overlay">
															<?php echo $item['description'] ?>
														 </span>
														<span class="title"><?php echo $item['title'] ?></span>
														<img src="<?php echo $image_arr[0] ?>" alt="">
													</a>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer() ?>
