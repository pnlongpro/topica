<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 12/31/2016
 * Time: 10:09 AM
 */

//Template Name: Home Page
?>

<?php get_header() ?>
<div class="nav-sticky-wrapper">
	<!-- LOCAL SECTION NAV -->
	<div class="container local-nav-wrapper">
		<nav class="row">
			<ul class="col-sm-12 local-nav">
				<li><a href="#your-education" class="">Cử nhân quốc tế Anh</a>
				</li>
				<li><a href="#your-education2">Cử nhân quốc tế Mỹ</a>
				</li>
				<li><a href="#academics-compare">Điểm mạnh của Topica IVY UNI</a>
				</li>
				<li><a href="#our-research">Các chương trình ưu đãi</a>
				</li>
			</ul>
		</nav>
	</div>
</div>

<div id="main-content">
	<!-- EDUCATION SECTION -->
	<section id="your-education" class="academics" aria-label="Your Education">
		<div class="container">
			<div class="row">
				<header class="col-sm-10 col-sm-offset-1">
					<div class="academics-header row">
						<h1 id="tier-1"><?php echo get_field( 'home_top_heading' ) ?></h1>
						<div class="col-sm-8 col-sm-offset-2"><?php echo get_field( 'home_top_description' ) ?></div>
					</div>
				</header>
			</div>
			<?php
			$list1 = get_field( 'home_UK_chuong_trinh_dao_tao' );
			if ( $list1 ) :
				?>
				<div class="row">
					<div class="info-boxes">
						<?php foreach ( $list1 as $value ) :
							$link = $value['link'] != '' ? $value['link'] : '#';
							$title = $value['title'] != '' ? $value['title'] : '';
							$description = $value['description'] != '' ? $value['description'] : '';
							$image = $value['image'] != '' ? $value['image'] : '';
							?>
							<a href="<?php echo $link ?>" class="box col-sm-4">
								<div class="box-img">
									<?php if ( $image ) : ?>
										<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
									<?php endif; ?>
									<div class="box-overlay"><?php echo $description ?></div>
								</div>
								<div class="box-caption">
									<p style="color:#033564;"><?php echo $title ?></p>
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="academics-info">
						<div class="row">
							<article class="col-md-6 col-sm-12">
								<h2><?php echo get_field( 'home-_bottom_heading' ) ?></h2>
								<?php echo get_field( 'home_bottom_description' ) ?>
							</article>
							<aside class="col-md-6 col-sm-12">
								<div class="academics-small-wrapper">
									<div class="academics-small-info row">
										<div class="academics-small-item col-sm-6 academics-border-right">
											<span><span class="countFade countUp countNoComma"><?php echo get_field( 'home_counter_1' ) ?></span></span>Year Founded
										</div>
										<div class="academics-small-item col-sm-6">
											<span><span class="countFade countUp"><?php echo get_field( 'home_counter_2' ) ?></span></span>Nat'l Merit Scholars
										</div>
									</div>
									<div class="academics-small-info row">
										<div class="academics-small-item col-sm-6 academics-border-right">
											<span class="countFade"><span class="countUp"><?php echo get_field( 'home_counter_3' ) ?></span>+</span>Degree Programs
										</div>
										<div class="academics-small-item col-sm-6">
											<span class="countFade">$<span class="countUp"><?php echo get_field( 'home_counter_4' ) ?></span>M</span>Total Research Funding
										</div>
									</div>
								</div>
							</aside>
						</div>
					</div>
				</div>
			</div>
			<section id="your-education2" class="academics" aria-label="Your Education2">
				<div class="container">
					<div class="row">
						<header class="col-sm-10 col-sm-offset-1">
							<div class="academics-header row">
								<h1 id="tier-1"><?php echo get_field( 'home_top_heading_2' ) ?></h1>

								<div class="col-sm-8 col-sm-offset-2"><?php echo get_field( 'home_top_description_2' ) ?></div>
							</div>
						</header>
					</div>
					<?php
					$list2 = get_field( 'home_US_chuong_trinh_dao_tao' );
					if ( $list2 ) :
						?>
						<div class="info-boxes">
							<?php foreach ( $list2 as $value ) :
								$link = $value['link'] != '' ? $value['link'] : '#';
								$title = $value['title'] != '' ? $value['title'] : '';
								$description = $value['description'] != '' ? $value['description'] : '';
								$image = $value['image'] != '' ? $value['image'] : '';
								?>
								<a href="<?php echo $link ?>" class="box col-sm-4">
									<div class="box-img">
										<?php if ( $image ) : ?>
											<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
										<?php endif; ?>
										<div class="box-overlay"><?php echo $description ?></div>
									</div>
									<div class="box-caption">
										<p style="color:#033564;"><?php echo $title ?></p>
									</div>
								</a>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<section id="academics-compare" class="academics-compare row" aria-label="Compare Academics">
						<div class="col-sm-10 col-sm-offset-1">
							<h3><?php echo get_field( 'home_top_heading_3' ) ?></h3>

							<div class="row">
								<div class="comparison-item academics-border-right col-sm-4 col-sm-offset-2">Tiết kiệm lên đến
									<h4 class="countFade" style="opacity: 1;"><?php echo get_field( 'home_counter_5' ) ?></h4>

								</div>
								<div class="comparison-item col-sm-4">Over 4 years, that's
									<h4 class="countFade" style="opacity: 1;"><?php echo get_field( 'home_counter_6' ) ?></h4>

								</div>
							</div>
							<div class="tuition-savings row">
								<div class="savings-header"><?php echo get_field( 'home_bottom_heading_3' ) ?></div>
								<?php
								$list_iconbox = get_field( 'home_icon_box' );
								if ( $list_iconbox ) :
									?>
									<?php foreach ( $list_iconbox as $key => $value ) : ?>
									<?php
									if ( $key == 0 ) {
										$class = 'savings-item col-sm-2 col-sm-offset-3';
									} else {
										$class = 'savings-item col-sm-2';
									}
									?>
									<div class="<?php echo esc_html( $class ) ?>">
										<?php echo $value['icon'] ?>

										<div class="label"><?php echo $value['title'] ?></div>
									</div>
								<?php endforeach; ?>
								<?php endif; ?>
							</div>
						</div>
					</section>
					<!-- OUR RESEARCH -->
					<section id="our-research" class="our-research" aria-label="Our Research">
						<div class="container">
							<div class="col-sm-10 col-sm-offset-1">
								<div class="row intro">
									<header class="col-sm-8 col-sm-offset-2 text-center">
										<h2>
											<?php echo get_field( 'home_top_heading_4' ) ?>
										</h2>
										<?php echo get_field( 'home_top_description_4' ) ?>
									</header>
								</div>
								<?php $list_4 = get_field( 'home_list_post' ); ?>
								<?php if ( $list_4 ) : ?>
									<div class="row content-wrap">
										<div class="content-row blue col-sm-12">
											<?php foreach ( $list_4 as $key => $value ) : ?>
												<?php
//												if( $key%2==0 ){
//													$class = 'col-sm-7';
//												}else {
//													$class = 'col-sm-7 col-sm-push-5';
//												}
												$class1 = $key % 2 == 0 ? 'col-sm-7' : 'col-sm-7 col-sm-push-5';
												$class2 = $key % 2 == 0 ? 'section-text col-sm-5' : 'section-text col-sm-5 col-sm-pull-7';
												$title = $value['title'] != '' ? $value['title'] : '';
												$description = $value['description'] != '' ? $value['description'] : '';
												$image = $value['image'] != '' ? $value['image'] : '';
												$link = $value['link'] != '' ? $value['link'] : '#';
												?>
												<div class="row">
													<div class="<?php echo $class1 ?>">
														<?php if( $image != '' ) : ?>
														<img src="<?php echo $image['url'] ?>" alt=""<?php echo $image['title'] ?>">
														<?php endif; ?>
													</div>
													<div class="<?php echo $class2 ?>">
														<h3>"<?php echo $title ?></h3>

														<div class="text-color" style="color:#868686;"><?php echo $description ?></div>
														<a href=<?php echo $link ?>" class="btn btn-blue">Tìm hiểu thêm</a>

													</div>
												</div>
											<?php endforeach; ?>
										</div>

									</div>
								<?php endif; ?>
							</div>
						</div>
					</section>
				</div>
			</section>
		</div>
	</section>

</div>
<!-- CTA -->

<?php get_footer() ?>
