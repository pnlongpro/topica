<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 12/31/2016
 * Time: 10:09 AM
 */

//Template Name: About US
?>

<?php get_header() ?>
<div class="nav-sticky-wrapper">
	<!-- LOCAL SECTION NAV -->
	<div class="container local-nav-wrapper">
		<nav class="row">
			<ul class="col-sm-12 local-nav">
				<li><a href="#interactive-tour">Topica IVY UNI</a>
				</li>
				<li><a href="#education">Trải nghiệm Sinh viên</a>
				</li>
				<li><a href="#visiting-campus">Chương trình Đào tạo</a>
				</li>
				<li><a href="#history">Quá trình phát triển</a>
				</li>
				<li><a href="#destination">Đăng ký</a>
				</li>
			</ul>
		</nav>
	</div>
</div>

<div id="main-content">
	<!-- INTERACTIVE TOUR -->
	<section id="interactive-tour" data-trigger-anim="anim_1" class="interactive-tour row" aria-label="Interactive Tour Section">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<header class="intro">
						<h2><?php echo get_field( 'aboutus-heading-1' ) ?></h2>

						<?php echo get_field( 'aboutus-description-1' ) ?>
						<a href="<?php echo get_field( 'aboutus-link-1' ) ?>" class="btn btn-blue" data-track="event" data-cat="interaction" data-act="button body" data-lab="take the inveractive tour">
							TÌM HIỂU NGAY
						</a>

					</header>
				</div>
				<div class="col-sm-7 tour-screenshot">
					<?php
					$image = get_field( 'aboutus-image-1' );
					?>
					<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>" class="anim_1">
				</div>
			</div>
		</div>
	</section>
	<!-- Top-Tier EXPERIENCE -->
	<section id="education" class="about-education row" aria-label="Top-Tier Experience section">
		<div class="container">
			<header class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<h2><?php echo get_field( 'aboutus-heading-2' ) ?></h2>

				</div>
			</header>
			<div class="experience-video row">
				<div class="col-sm-7"><?php echo get_field( 'aboutus-description-2' ) ?>.</div>
				<div class="col-sm-5">
					<a href="<?php echo get_field( 'aboutus-video-url' ) ?>" class="wistia-popover[height=450,playerColor=7b796a,width=800] video-wrapper">
						<?php $img = get_field( 'aboutus-image-2' ) ?>
						<img src="<?php echo $img['url'] ?>" alt="<?php echo $img['title'] ?>">
						<div class="video-caption">
							<?php echo get_field( 'aboutus-video_caption' ) ?>
						</div>
						<span class="btn-outer"><span class="btn-inner">&nbsp;</span></span>
					</a>
				</div>
			</div>
			<div class="education-points row">
				<h3 class="col-sm-12"><?php echo get_field( 'aboutus-heading-3' ) ?></h3>
				<?php
				$list_info = get_field( 'aboutus_info_boxs' );
				if ( $list_info ) :
				?>
				<div class="info-boxes">
					<?php
					foreach ( $list_info as $key => $value ) :
					if ( $key == 4 ) {
						?>
						<div class="box box-empty col-sm-4">
						</div>
						<?php
					}
					$image       = $value['thumbnail'];
					$title       = $value['title'];
					$description = $value['description'];
					?>
					<div class="box col-sm-4">
						<div class="box-img">
							<img class="img-responsive" src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'] ?>">
							<div class="box-overlay"
							<?php echo $description ?>
						</div>
					</div>
					<div class="box-caption"><span><?php echo $key + 1 ?></span>
						<p><?php echo $title ?></p>
					</div>
				</div>
			<?php endforeach; ?>
			</div>
			<?php endif; ?>
		</div>
</div>
</section>
<!-- VISITING CAMPUS -->
<section id="visiting-campus" class="visiting-campus row" aria-label="Visiting Campus section">
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1">
			<header class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<h2><?php echo get_field( 'aboutus-heading-4' ) ?></h2>
				</div>
			</header>
			<?php
			$list_info_2 = get_field( 'aboutus-chuong-trinh-dao-tao' );
			if ( $list_info_2 ) :
				?>
				<div class="col-sm-10 col-sm-offset-1">
					<!-- PHOTOBOX -->
					<ul id="photoBox" class="campus-photo-box">
						<?php
						foreach ( $list_info_2 as $item ) :
							$link = $item['link'] != '' ? $item['link'] : '#';
							$title = $item['title'] != '' ? $item['title'] : '';
							$sub_title = $item['sub-title'] != '' ? $item['sub-title'] : '';
							$thumbnail = $item['thumbnail'] != '' ? $item['thumbnail'] : '';
							$bg_color = $item['background_color'] != '' ? $item['background_color'] : '';
							$style = '';
							if ( $bg_color != '' ) {
								$style = 'style="background-color: ' . $bg_color . '"';
							}
							?>
							<li>
								<a href="<?php echo $link; ?>" class="orange-box">
									<img src="<?php echo $thumbnail['url'] ?>" alt="<?php echo $thumbnail['title'] ?>" />
									<span class="photo-label-wrap">
										<span class="photo-label top-left"><?php echo $title ?></span>
									</span>
									<span class="overlay-photo"></span>
									<span class="overlay-text" <?php echo $style ?>>
										<span class="ov-header"><?php echo $title ?></span>
									 	<span class="ov-text"><?php echo $sub_title ?></span>
									  	<span class="ov-btn btn">TÌM HIỂU THÊM</span>
									</span>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<!-- HISTORY SECTION -->
<section id="history" class="unr-history row" aria-label="History Section">
	<div class="container">
		<header class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2><?php echo get_field( 'aboutus-heading-5' ) ?></h2>

				<?php echo get_field( 'aboutus_description_5' ) ?>
				<a href="<?php echo get_field( 'aboutus_button_link' ) ?>" class="btn">History, Stats &amp; Highlights</a>

			</div>
		</header>
		<?php
		$about_gallery = get_field( 'aboutus_gallery' );
		if ( $about_gallery ) :
			?>
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="history-slider flexslider">
						<ul class="slides">
							<?php
							foreach ( $about_gallery as $value ) :
								?>
								<li>
									<img src="<?php echo $value['url'] ?>" alt="<?php echo $value['title'] ?>">
								</li>
							<?php endforeach; ?>

						</ul>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<!-- FULLWIDTH SLIDER -->
<div id="destination" class="row rt-slider">
	<?php
	$register_gallery = get_field( 'aboutus_gallery_6' );
	if ( $register_gallery ) :
		?>
		<div class="full-slider flexslider">
			<ul class="slides">
				<?php foreach ( $register_gallery as $value ) : ?>
					<li>
						<img src="<?php echo $value['url'] ?>" alt="<?php echo $value['title'] ?>">
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
	<div class="slider-overlay">
		<h3><?php echo get_field( 'aboutus_heading_6' ) ?></h3>
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<?php echo get_field( 'aboutus_description_6' ) ?>
			</div>
		</div>
		<a href="<?php echo get_field( 'aboutus_button_link_6' ) ?>" class="btn special-btn sm-hide">Đăng ký</a>
		<a href="<?php echo get_field( 'aboutus_button_link_6' ) ?>" class="btn sm-show" style="display:none">Đăng ký</a>

	</div>
</div>

</div>

<script>
	$(document).ready(function () {
		aboutPage();
	});
</script>


<?php get_footer() ?>
