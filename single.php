<?php get_header(); ?>

<div id="content" class="vlog-site-content">

	<article id="post-90" class="post-90 post type-post status-publish format-standard has-post-thumbnail hentry category-technology tag-magazine tag-video">

		<div class="vlog-featured vlog-featured-1 vlog-single-cover">

			<div class="vlog-featured-item">

				<div class="vlog-cover-bg standard">

					<div class="vlog-cover">
						<?php
						$thumbnail_url = '';
						$width = 1500;
						$height = 500;
						$image = get_post_thumbnail_id( get_the_ID() );
						if (!empty($image)) {
							$images_attr = wp_get_attachment_image_src($image, "full");
							if (isset($images_attr)) {
								$resize = matthewruddy_image_resize($images_attr[0], $width, $height);
								if ($resize != null)
									$thumbnail_url = $resize['url'];
							}
						}
						?>
						<img width="1500" height="500" src="<?php echo $thumbnail_url ?>" class="attachment-vlog-cover-full size-vlog-cover-full wp-post-image" alt="vlog005">
					</div>

					<?php get_template_part('template-parts/post','nav') ?>

				</div>

				<div class="vlog-featured-info container vlog-cover-hover-mode vlog-f-hide">

					<div class="row">

						<div class="col-lg-12">

							<div class="vlog-featured-info-bg vlog-highlight">

								<div class="entry-header">

									<span class="entry-category">
										<?php echo get_the_category_list(' / '); ?>
									</span>

									<h1 class="entry-title"><?php the_title() ?></h1>

									<?php get_template_part('template-parts/post','meta') ?>

								</div>

								<div class="entry-actions vlog-vcenter-actions">
									<?php if ( comments_open() || get_comments_number() ) : ?>
										<div class="meta-item meta-comments">
											<?php comments_popup_link(wp_kses_post(__('0 <span>Bình luận</span> ','topica')) ,wp_kses_post(__('1 <span>Bình luận</span> ','topica')),wp_kses_post(esc_html__('% <span>Bình luận</span>','topica'))); ?>
										</div>
									<?php endif; ?>
								</div>

							</div>

						</div>

					</div>

				</div>

				<div class="vlog-format-inplay vlog-bg">
					<div class="container">
					</div>
				</div>
			</div>
		</div>
		<div class="vlog-section ">

			<div class="container">

				<div class="vlog-content vlog-single-content">

					<div class="entry-content-single">
						<?php get_template_part('template-parts/post','share'); ?>

						<?php the_content(); ?>

						<?php  the_tags('<div class="meta-tags tagcloud"><label>'.esc_html__('Tags','topica') .' :</label>', '', '</div>');?>
						<?php  comments_template(); ?>
					</div>
					<div class="vlog-sidebar vlog-sidebar-right">
						<?php dynamic_sidebar('widget-area-1') ?>
					</div>

					</div>

				</div>



			</div>

		</div>

	</article>

</div>

<?php get_footer(); ?>
